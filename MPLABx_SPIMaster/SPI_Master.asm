;*****************;Firmware para tarjeta Hideyo que env�a datos v�a SPI maestro a 12MHz a
; una segunda curiosity de 16 bits, para se guardada dentro de un tarjeta microsd.
; No utiliza USB (Universal Serial Bus) usando transferencias bulk.
; El programa arranca y activa su puerto SPI en modo maestro 1,1. 
; Activa el pull_up interno de los puertos B y espera que RB2 (push botton de inicio de inicio) pase a bajo.
; Luego el programa constantemente se asegura que la se�al de RB2 contin�e en alto ya que si el push_botton es presionado de nuevo
; el SPI maestro dejara de transmitir
; La salida de datos corresponde al puesto MOSI (RC7).
; Tambi�n se enviara una se�al en el puerto RA4 que indica que al SPI esclavo que el maestro todav�a esta enviado datos.
; El puerto RA0 servir� de indicador visual de la operaci�n (led activo significa envio de datos, led inactivo no env�a).
; Una vez que comienza el proceso de transmisi�n con la primera activaci�n de RB2
; Se proceder� a verificar si alguno de los 2 buffers de datos tiene un paquete de datos completo para enviar.
; Cada 2 ms una interrupci�n simula la recopilaci�n de datos de los sensores de una prueba de polisomnografia.
; debido a que los tipos de sensores de env�a a diferentes frecuencias se configuran 10 estados para la simulaci�n de
; recopilaci�n de datos de los sensores.
; Cada uno de los 10 estados de envio es descrito en base a la siguiente tabla.
;************************************************************************************
;	2ms	4ms	6ms	8ms	10ms	12ms	14ms	16ms	18ms	20ms
;EMG	|	|	|	|	|	|	|	|	|	|
;EEG		|		|		|		|		|
;IMUs					|					|
;*************************************************************************************
; Los sensores de Electromiograf�a (EMG) son tres cada uno manda tres bytes de informaci�n (low,mid,high).
; Los sensores de Electro encefalograf�a son seis igualmente cada uno manda 3 bytes de informaci�n (low,mid,high).
; Los sensores IMUS env�an 3 tipos de informaci�n diferente (aceler�metro,  giroscopio, magnet�metro) cada tipo de informaci�n
; cada tipo de informacion tiene 3 ejes de 2 bytes por eje.  
; Una vez que el impulso fue simulado, se incrementan los registros de informaci�n de cada sensor que 
;sea parte del estado que se envi� en ese momento.
; La informaci�n simulada que se est� obteniendo, se guarda en dos buffers de 512 bytes 
;(FSR0, del 0x200 al 0x400 para el buffer 1 y del 0x401 al 0x600 para el buffer 2), 
; al llenarse alguno de los 2 este env�a toda la informaci�n en este pone alto una bandera y la informaci�n 
; se empieza a enviar en el otro.
; Debido a que el proceso es as�ncrono se tiene un buffer especial para rebosos (FSR2), 
; donde se guardan todos los datos de rebose para posteriormente
; ser enviados al inicio de alguno de los 2 buffers. 
; En cada uno de los estados se checa en que buffer de envi� se deben guardar los datos al igual, 
; si el buffer ya tiene los 512 para enviar ese paquete 
; y si hay un rebose para guardarlos primero en el buffer auxiliar y luego al inicio del siguiente buffer.
; Cada envio de un paquete de datos hace que un registro de tres BYTES llamado ?cuenta? (cuenta, cuenta1, cuenta2) 
; crezca hasta alcanzar el tama�o 
; de 1285735 bits que es aproximadamente el n�mero de paquete que se recopilan en una prueba de 10 hrs de polisognografia.
; Una vez que se llega al n�mero deseado de paquete de datos enviado la bandera ?final? se pone alto e 
; indica al SPI maestro que deje de enviar datos al SPI esclavo y que detenga el proceso de simulaci�n.
; El microcontrolador utiliza un resonador de 8 MHz para configurar un reloj interno con una 
; frecuencia de 48 MHz.
; Cada instrucci�n se ejecuta en 83.33ns (0.08333us)
;
; 
;**************************************************************************************************
	list p=PIC18F2550
	radix HEX
	#include <P18F2550.INC>
	
	errorlevel -302

; CONFIG1L
  CONFIG  PLLDIV = 2            ; PLL Prescaler Selection bits (Divide by 2 (8 MHz oscillator input))
  CONFIG  CPUDIV = OSC1_PLL2    ; System Clock Postscaler Selection bits ([Primary Oscillator Src: /1][96 MHz PLL Src: /2])
  CONFIG  USBDIV = 2            ; USB Clock Selection bit (used in Full-Speed USB mode only; UCFG:FSEN = 1) (USB clock source comes from the 96 MHz PLL divided by 2)

; CONFIG1H
  CONFIG  FOSC = HSPLL_HS       ; Oscillator Selection bits (HS oscillator, PLL enabled (HSPLL))
  CONFIG  FCMEN = OFF           ; Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
  CONFIG  IESO = OFF            ; Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

; CONFIG2L
  CONFIG  PWRT = ON             ; Power-up Timer Enable bit (PWRT enabled)
  CONFIG  BOR = ON_ACTIVE       ; Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only and disabled in Sleep mode (SBOREN is disabled))
  CONFIG  BORV = 3              ; Brown-out Reset Voltage bits (Minimum setting 2.05V)
  CONFIG  VREGEN = ON           ; USB Voltage Regulator Enable bit (USB voltage regulator enabled)

; CONFIG2H
  CONFIG  WDT = OFF             ; Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
  CONFIG  WDTPS = 128           ; Watchdog Timer Postscale Select bits (1:128)

; CONFIG3H
  CONFIG  CCP2MX = ON           ; CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
  CONFIG  PBADEN = OFF          ; PORTB A/D Enable bit (PORTB<4:0> pins are configured as digital I/O on Reset)
  CONFIG  LPT1OSC = OFF         ; Low-Power Timer 1 Oscillator Enable bit (Timer1 configured for higher power operation)
  CONFIG  MCLRE = ON            ; MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

; CONFIG4L
  CONFIG  STVREN = OFF          ; Stack Full/Underflow Reset Enable bit (Stack full/underflow will not cause Reset)
  CONFIG  LVP = OFF             ; Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
  CONFIG  XINST = OFF           ; Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

; CONFIG5L
  CONFIG  CP0 = OFF             ; Code Protection bit (Block 0 (000800-001FFFh) is not code-protected)
  CONFIG  CP1 = OFF             ; Code Protection bit (Block 1 (002000-003FFFh) is not code-protected)
  CONFIG  CP2 = OFF             ; Code Protection bit (Block 2 (004000-005FFFh) is not code-protected)
  CONFIG  CP3 = OFF             ; Code Protection bit (Block 3 (006000-007FFFh) is not code-protected)

; CONFIG5H
  CONFIG  CPB = OFF             ; Boot Block Code Protection bit (Boot block (000000-0007FFh) is not code-protected)
  CONFIG  CPD = OFF             ; Data EEPROM Code Protection bit (Data EEPROM is not code-protected)

; CONFIG6L
  CONFIG  WRT0 = OFF            ; Write Protection bit (Block 0 (000800-001FFFh) is not write-protected)
  CONFIG  WRT1 = OFF            ; Write Protection bit (Block 1 (002000-003FFFh) is not write-protected)
  CONFIG  WRT2 = OFF            ; Write Protection bit (Block 2 (004000-005FFFh) is not write-protected)
  CONFIG  WRT3 = OFF            ; Write Protection bit (Block 3 (006000-007FFFh) is not write-protected)

; CONFIG6H
  CONFIG  WRTC = OFF            ; Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) are not write-protected)
  CONFIG  WRTB = OFF            ; Boot Block Write Protection bit (Boot block (000000-0007FFh) is not write-protected)
  CONFIG  WRTD = OFF            ; Data EEPROM Write Protection bit (Data EEPROM is not write-protected)

; CONFIG7L
  CONFIG  EBTR0 = OFF           ; Table Read Protection bit (Block 0 (000800-001FFFh) is not protected from table reads executed in other blocks)
  CONFIG  EBTR1 = OFF           ; Table Read Protection bit (Block 1 (002000-003FFFh) is not protected from table reads executed in other blocks)
  CONFIG  EBTR2 = OFF           ; Table Read Protection bit (Block 2 (004000-005FFFh) is not protected from table reads executed in other blocks)
  CONFIG  EBTR3 = OFF           ; Table Read Protection bit (Block 3 (006000-007FFFh) is not protected from table reads executed in other blocks)

; CONFIG7H
  CONFIG  EBTRB = OFF           ; Boot Block Table Read Protection bit (Boot block (000000-0007FFh) is not protected from table reads executed in other blocks)
;
;
;
;
;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;Inicia	declaraci�n de variables
cont		equ 	0x060
aux 		equ 	0x061
offset 		equ 	0x062
aux_USTAT	equ 	0x063

;************************************************************
;Registro para estados del USB							  	
;		bit 7 (ADDAS) = Asignar direcci�n del dispositivo USB
;			1 - Se debe asignar la direcci�n del dispositivo USB.			
;			0 - No se debe asignar la direcci�n del dispositivo USB.						
;		bit 6 (SUSP) = Estado SUSPENDED						
;			1 - Se encuentra en estado SUSPENDED.			
;			0 - No se encuentra en estado SUSPENDED.		
;		bit 5 (CONF) = Estado CONFIGURED					
;			1 - Se encuentra en estado CONFIGURED.
;			0 - No se encuentra en estado CONFIGURED.
;		bit 4 (ADDR) = Estado ADDREES
;			1 - Se encuentra en estado ADDREES.
;			0 - No se encuentra en estado ADDREES.
;		bit 3 (DEF) = Estado DEFAULT
;			1 - Se encuentra en estado DEFAULT.
;			0 - No se encuentra en estado DEFAULT.
;		bit 2 (POW) = Estado POWERED
;			1 - Se encuentra en estado POWERED.
;			0 - No se encuentra en estado POWERED.
;		bit 1 (ATTA) = Estado ATTACHED
;			1 - Se encuentra en estado ATTACHED.
;			0 - No se encuentra en estado ATTACHED.
;		bit 0 (NODS) = No Data Stage
;			1 - La solicitud actual NO contiene Data Stage. 
;			0 - La solicitud actual SI contiene Data Stage. 
USB_REG		equ 0x064
#define 	NODS  	0
#define 	ATTA 	1
#define 	POW	2
#define 	DEF	3
#define 	ADDR 	4
#define 	CONF 	5
#define 	SUSP 	6
#define 	ADDAS	7

;***********************************************************
;
;************************************************************
;Registro 2 para estados del USB							  	
;		bit 7 Unimplemented: Read as ?0?
;		bit 6 Unimplemented: Read as ?0?
;		bit 5 Unimplemented: Read as ?0?
;		bit 4 Unimplemented: Read as ?0?
;		bit 3 (ZPCKT) = Zero Packet
;			1 - Se debe enviar un paquete de longitud cero en la siguiente transacci�n IN.
;			0 - No se debe enviar un paquete de longitud cero.
;		bit 2 (PRDES) = Primer descriptor
;			1 - Es la primera solicitud de GET_DESCRIPTOR.
;			0 - No es la primera solicitud.
;		bit 1 (DEBUG) = Depurar
;			1 - Depurar.
;			0- No depurar
;		bit 0 (DTSACT) = Data Toggle Synchronization actual
;			1 - DATA 1. 
;			0 - DATA 0. 
USB_REG2	equ 0x065
#define 	DTSACT  0
#define 	DEBUG	1
#define 	PRDES	2
#define 	ZPCKT	3
#define		FRSTIN  5
;
;************************************************************
;Registro							  	
;		bit 7 Unimplemented: Read as ?0?
;		bit 6 Unimplemented: Read as ?0?
;		bit 5 Unimplemented: Read as ?0?
;		bit 4 Unimplemented: Read as ?0?
;		bit 3 Unimplemented: Read as ?0?
;		bit 2 Unimplemented: Read as ?0?
;		bit 1 (EP1_IN_PPBI) = Indica en que buffer ping-pong fue la �ltima transferencia del IN del EP1.
;			1 - Fue en el buffer impar.
;			0 - Fue en el buffer par.
;		bit 0 (EP1_OUT_PPBI) = Indica en que buffer ping-pong fue la �ltima transferencia del OUT del EP1.
;			1 - Fue en el buffer impar.
;			0 - Fue en el buffer par.
;USB_REG3	equ 0x066
;#define 	EP1_OUT_PPBI  	0
;#define 	EP1_IN_PPBI 	1
;***********************************************************
;
;TRNF_REG	equ 0x067
;#define 	PC_Dato 0	
;***********************************************************
;
W_TEMP		equ 0x068
STATUS_TEMP	equ 0x069
BSR_TEMP	equ 0x06A
;NoBytes		equ 0x06B
;Dummy   	equ 0x06C
;cont_tmr1	equ 0x06D
;cont_seg	equ 0x06E
;DH		equ 0x06F
;DL		equ 0x070
;;
;Menu		equ 0x071
;#define		TMR1_RUN 0
;#define		CLR_TMR1 1
;#define		Negativo 2
;#define         PackOne 3
;#define         Termino 4
;;
BUFI		equ 0x072
;	
;;*********************---USB---*********************************
;;BD0: buffer descriptor para OUT del EP0
;BD0STAT 		equ 	0x400
;BD0CNT 			equ 	0x401
;BD0ADRL			equ 	0x402
;BD0ADRH			equ 	0x403
;;BD1: buffer descriptor para IN del EP0
;BD1STAT 		equ 	0x404
;BD1CNT 			equ 	0x405
;BD1ADRL			equ 	0x406
;BD1ADRH			equ 	0x407
;;BD2: buffer descriptor para OUT (buffer par) del EP1
;BD2STAT 		equ 	0x408
;BD2CNT 			equ 	0x409
;BD2ADRL			equ 	0x40A
;BD2ADRH			equ 	0x40B
;;BD3: buffer descriptor para OUT (buffer impar) del EP1
;BD3STAT 		equ 	0x40C
;BD3CNT 			equ 	0x40D
;BD3ADRL			equ 	0x40E
;BD3ADRH			equ 	0x40F
;;BD2: buffer descriptor para IN (buffer par) del EP1
;BD4STAT 		equ 	0x410
;BD4CNT 			equ 	0x411
;BD4ADRL			equ 	0x412
;BD4ADRH			equ 	0x413
;;BD3: buffer descriptor para IN (buffer impar) del EP1
;BD5STAT 		equ 	0x414
;BD5CNT 			equ 	0x415
;BD5ADRL			equ 	0x416
;BD5ADRH			equ 	0x417
;;
;
;Registros para almacenar datos provenientes de la PC v�a USB:
;Los datos que lleguen de la PC a EP1 out par o impar se almacenan en 0x100-0x140
;
;OUT del EP0                  0x500
;IN del EP0                   0x540
;
;OUT (buffer par) del EP1     0x580
;OUT (buffer impar) del EP1   0x5C0
;
;Registros para almacenar datos para enviar a la PC:
;IN (buffer par) del EP1      0x600
;IN (buffer impar) del EP1    0x640
;
;
;
;***********************************************************
;Secci�n para declarar variables para la rutina a implementar.
;De las direcciones 0x80 a la 0x9F
;
;Variables para los canales 0 al 2 de EMG
C0emgLo			equ	0x080
C0emgMd			equ	0x081
C0emgHi			equ	0x082
C1emgLo			equ	0x083
C1emgMd			equ	0x084	    ;
C1emgHi			equ     0x085
C2emgLo			equ	0x086
C2emgMd			equ	0x087
C2emgHi			equ	0x088
;
;Variables para los canales 0 al 5 de EEG
C0eegLo			equ	0x089
C0eegMd			equ	0x08A
C0eegHi			equ	0x08B
C1eegLo			equ	0x08C
C1eegMd			equ	0x08D	    ;
C1eegHi			equ     0x08E
C2eegLo			equ	0x08F
C2eegMd			equ	0x090
C2eegHi			equ	0x091
C3eegLo			equ	0x092
C3eegMd			equ	0x093
C3eegHi			equ	0x094
C4eegLo			equ	0x095
C4eegMd			equ	0x096	    ;
C4eegHi			equ     0x097
C5eegLo			equ	0x098
C5eegMd			equ	0x099
C5eegHi			equ	0x09A			
;
;Variables para las 6 IMUs. Se declara solamente para la imu 1, se reutilizan estas variables para las imus 2 a 6
M1AXLo			equ	0x09B	    ;imu 1 aceler�metro eje x byte bajo
M1AXHi			equ	0x09C	    ;imu 1 aceler�metro eje x byte alto
M1AYLo			equ	0x09D	    ;imu 1 aceler�metro eje y byte bajo
M1AYHi			equ	0x09E	    ;imu 1 aceler�metro eje y byte alto
M1AZLo			equ	0x09F	    ;imu 1 aceler�metro eje z byte bajo
M1AZHi			equ	0x100	    ;imu 1 aceler�metro eje z byte alto
M1GXLo			equ	0x101	    ;imu 1 giroscopio eje x byte bajo
M1GXHi			equ	0x102	    ;imu 1 giroscopio eje x byte alto
M1GYLo			equ	0x103	    ;imu 1 giroscopio eje y byte bajo
M1GYHi			equ	0x104	    ;imu 1 giroscopio eje y byte alto
M1GZLo			equ	0x105	    ;imu 1 giroscopio eje z byte bajo
M1GZHi			equ	0x106	    ;imu 1 giroscopio eje z byte alto			
M1MXLo			equ	0x107	    ;imu 1 magnet�metro eje x byte bajo
M1MXHi			equ	0x108	    ;imu 1 magnet�metro eje x byte alto
M1MYLo			equ	0x109	    ;imu 1 magnet�metro eje y byte bajo
M1MYHi			equ	0x10A	    ;imu 1 magnet�metro eje y byte alto			
M1MZLo			equ	0x10B	    ;imu 1 magnet�metro eje z byte bajo
M1MZHi			equ	0x10C	    ;imu 1 magnet�metro eje z byte alto			
;
;variables para rutinas matem�ticas
TmpMd			equ	0x10D
TmpHi			equ	0x10E
TmpVH			equ	0x10F
;
ACCaLo			equ	0x110
ACCaMd			equ	0x111
ACCaHi			equ	0x112
ACCaVH			equ	0x113
ACCbLo			equ	0x114
ACCbMd			equ	0x115
ACCbHi			equ	0x116
ACCbVH			equ	0x117
;
Estados			equ	0x118
Banderas		equ	0x119
#define		TxBuff1	    Banderas,1
#define		TxBuff2	    Banderas,2
#define		Rebose	    Banderas,3
#define		Final	    Banderas,4
;
TempoL			equ	0x11A
TempoH			equ	0x11B
Cuenta			equ	0x11C
Cuenta1			equ	0x11D
Cuenta2			equ	0x11E
CualBuffer		equ	0x11F

DC0H			equ	0x120	; Parte alta canal C0 (0x0000 - 0x03FF) se envia cada 512 bytes (0x01FF - 0x03FF)
DC0L			equ	0x121	; Parte baja canal C0 (0x0000 - 0x03FF) se envia cada 512 bytes (0x01FF - 0x03FF)

DC1H			equ	0x122	; Parte alta canal C1 (0x0400 - 0x07FF) se envia cada 512 bytes (0x05FF - 0x07FF)
DC1L			equ	0x123	; Parte baja canal C1 (0x0400 - 0x07FF) se envia cada 512 bytes (0x05FF - 0x07FF)
			
DC2H			equ	0x124	; Parte alta canal C2 (0x0800 - 0x0BFF) se envia cada 512 bytes (0x09FF - 0x0BFF)
DC2L			equ	0x125	; Parte baja canal C2 (0x0800 - 0x0BFF) se envia cada 512 bytes (0x09FF - 0x0BFF)

DE0H			equ	0x126	; Parte alta canal E0 (0x0C00 - 0x0FFF) se envia cada 512 bytes (0x0DFF - 0x0FFF)
DE0L			equ	0x127	; Parte baja canal E0 (0x0C00 - 0x0FFF) se envia cada 512 bytes (0x0DFF - 0x0FFF)
			
DE1H			equ	0x128	; Parte alta canal E1 (0x1000 - 0x13FF) se envia cada 512 bytes (0x11FF - 0x13FF)
DE1L			equ	0x129	; Parte baja canal E1 (0x1000 - 0x13FF) se envia cada 512 bytes (0x11FF - 0x13FF)
			
DE2H			equ	0x12A	; Parte alta canal E2 (0x1400 - 0x17FF) se envia cada 512 bytes (0x15FF - 0x17FF)
DE2L			equ	0x12B	; Parte baja canal E2 (0x1400 - 0x17FF) se envia cada 512 bytes (0x15FF - 0x17FF)
			
DE3H			equ	0x12C	; Parte alta canal E3 (0x1800 - 0x1BFF) se envia cada 512 bytes (0x19FF - 0x1BFF)
DE3L			equ	0x12D	; Parte baja canal E3 (0x1800 - 0x1BFF) se envia cada 512 bytes (0x19FF - 0x1BFF)
			
DE4H			equ	0x12E	; Parte alta canal E4 (0x1C00 - 0x1FFF) se envia cada 512 bytes (0x1DFF - 0x1FFF)
DE4L			equ	0x12F	; Parte baja canal E4 (0x1C00 - 0x1FFF) se envia cada 512 bytes (0x1DFF - 0x1FFF)
			
DE5H			equ	0x130	; Parte alta canal E5 (0x2000 - 0x23FF) se envia cada 512 bytes (0x21FF - 0x23FF)
DE5L			equ	0x131	; Parte baja canal E5 (0x2000 - 0x23FF) se envia cada 512 bytes (0x21FF - 0x23FF)

DM1AH			equ	0x132	; Parte alta canal M1A (0x2400 - 0x27FF) se envia cada 512 bytes (0x25FF - 0x27FF)
DM1AL			equ	0x133	; Parte baja canal M1A (0x2400 - 0x27FF) se envia cada 512 bytes (0x25FF - 0x27FF)

DM1GH			equ	0x134	; Parte alta canal M1G (0x2800 - 0x2BFF) se envia cada 512 bytes (0x29FF - 0x2BFF)
DM1GL			equ	0x135	; Parte baja canal M1G (0x2800 - 0x2BFF) se envia cada 512 bytes (0x29FF - 0x2BFF)

DM1MH			equ	0x136	; Parte alta canal M1M (0x2C00 - 0x2FFF) se envia cada 512 bytes (0x2DFF - 0x2FFF)
DM1ML			equ	0x137	; Parte baja canal M1M (0x2C00 - 0x2FFF) se envia cada 512 bytes (0x2DFF - 0x2FFF)

DM2AH			equ	0x138	; Parte alta canal M2A (0x3000 - 0x33FF) se envia cada 512 bytes (0x31FF - 0x33FF)
DM2AL			equ	0x139	; Parte baja canal M2A (0x3000 - 0x33FF) se envia cada 512 bytes (0x31FF - 0x33FF)

DM2GH			equ	0x13A	; Parte alta canal M2G (0x3400 - 0x37FF) se envia cada 512 bytes (0x35FF - 0x37FF)
DM2GL			equ	0x13B	; Parte baja canal M2G (0x3400 - 0x37FF) se envia cada 512 bytes (0x35FF - 0x37FF)

DM2MH			equ	0x13C	; Parte alta canal M2M (0x3800 - 0x3BFF) se envia cada 512 bytes (0x39FF - 0x3BFF)
DM2ML			equ	0x13D	; Parte baja canal M2M (0x3800 - 0x3BFF) se envia cada 512 bytes (0x39FF - 0x3BFF)
			
DM3AH			equ	0x13E	; Parte alta canal M3A (0x3C00 - 0x3FFF) se envia cada 512 bytes (0x3DFF - 0x3FFF)
DM3AL			equ	0x13F	; Parte baja canal M3A (0x3C00 - 0x3FFF) se envia cada 512 bytes (0x3DFF - 0x3FFF)

DM3GH			equ	0x140	; Parte alta canal M3G (0x4000 - 0x43FF) se envia cada 512 bytes (0x41FF - 0x43FF)
DM3GL			equ	0x141	; Parte baja canal M3G (0x4000 - 0x43FF) se envia cada 512 bytes (0x41FF - 0x43FF)

DM3MH			equ	0x142	; Parte alta canal M3M (0x4400 - 0x47FF) se envia cada 512 bytes (0x45FF - 0x47FF)
DM3ML			equ	0x143	; Parte baja canal M3M (0x4400 - 0x47FF) se envia cada 512 bytes (0x45FF - 0x47FF)
			
DM4AH			equ	0x144	; Parte alta canal M4A (0x4800 - 0x4BFF) se envia cada 512 bytes (0x49FF - 0x4BFF)
DM4AL			equ	0x145	; Parte baja canal M4A (0x4800 - 0x4BFF) se envia cada 512 bytes (0x49FF - 0x4BFF)

DM4GH			equ	0x146	; Parte alta canal M4G (0x4C00 - 0x4FFF) se envia cada 512 bytes (0x4DFF - 0x4FFF)
DM4GL			equ	0x147	; Parte baja canal M4G (0x4C00 - 0x4FFF) se envia cada 512 bytes (0x4DFF - 0x4FFF)

DM4MH			equ	0x148	; Parte alta canal M4M (0x5000 - 0x53FF) se envia cada 512 bytes (0x51FF - 0x53FF)
DM4ML			equ	0x149	; Parte baja canal M4M (0x5000 - 0x53FF) se envia cada 512 bytes (0x51FF - 0x53FF)
			
DM5AH			equ	0x14A	; Parte alta canal M5A (0x5400 - 0x57FF) se envia cada 512 bytes (0x55FF - 0x57FF)
DM5AL			equ	0x14B	; Parte baja canal M5A (0x5400 - 0x57FF) se envia cada 512 bytes (0x55FF - 0x57FF)

DM5GH			equ	0x14C	; Parte alta canal M5G (0x5800 - 0x5BFF) se envia cada 512 bytes (0x59FF - 0x5BFF)
DM5GL			equ	0x14D	; Parte baja canal M5G (0x5800 - 0x5BFF) se envia cada 512 bytes (0x59FF - 0x5BFF)

DM5MH			equ	0x14E	; Parte alta canal M5M (0x5C00 - 0x5FFF) se envia cada 512 bytes (0x5DFF - 0x5FFF)
DM5ML			equ	0x14F	; Parte baja canal M5M (0x5C00 - 0x5FFF) se envia cada 512 bytes (0x5DFF - 0x5FFF)			

DM6AH			equ	0x150	; Parte alta canal M2A (0x6000 - 0x63FF) se envia cada 512 bytes (0x61FF - 0x63FF)
DM6AL			equ	0x151	; Parte baja canal M2A (0x6000 - 0x63FF) se envia cada 512 bytes (0x61FF - 0x63FF)

DM6GH			equ	0x152	; Parte alta canal M2G (0x6400 - 0x67FF) se envia cada 512 bytes (0x65FF - 0x67FF)
DM6GL			equ	0x153	; Parte baja canal M2G (0x6400 - 0x67FF) se envia cada 512 bytes (0x65FF - 0x67FF)

DM6MH			equ	0x154	; Parte alta canal M2M (0x6800 - 0x6BFF) se envia cada 512 bytes (0x69FF - 0x6BFF)
DM6ML			equ	0x155	; Parte baja canal M2M (0x6800 - 0x6BFF) se envia cada 512 bytes (0x69FF - 0x6BFF)
			
FlagsC			equ	0x156
#define		B1C0	FlagsC,1
#define		B2C0	FlagsC,2
#define		B1C1	FlagsC,3
#define		B2C1	FlagsC,4
#define		B1C2	FlagsC,5
#define		B2C2	FlagsC,6
			
FlagsE1			equ	0x157
#define		B1E0	FlagsE1,1
#define		B2E0	FlagsE1,2
#define		B1E1	FlagsE1,3
#define		B2E1	FlagsE1,4
#define		B1E2	FlagsE1,5
#define		B2E2	FlagsE1,6
			
FlagsE2			equ	0x158
#define		B1E3	FlagsE2,1
#define		B2E3	FlagsE2,2
#define		B1E4	FlagsE2,3
#define		B2E4	FlagsE2,4
#define		B1E5	FlagsE2,5
#define		B2E5	FlagsE2,6
			
FlagsM1			equ	0x159
#define		B1M1A	FlagsM1,1
#define		B2M1A	FlagsM1,2
#define		B1M1G	FlagsM1,3
#define		B2M1G	FlagsM1,4
#define		B1M1M	FlagsM1,5
#define		B2M1M	FlagsM1,6

FlagsM2			equ	0x15A
#define		B1M2A	FlagsM2,1
#define		B2M2A	FlagsM2,2
#define		B1M2G	FlagsM2,3
#define		B2M2G	FlagsM2,4
#define		B1M2M	FlagsM2,5
#define		B2M2M	FlagsM2,6
			
FlagsM3			equ	0x15B
#define		B1M3A	FlagsM3,1
#define		B2M3A	FlagsM3,2
#define		B1M3G	FlagsM3,3
#define		B2M3G	FlagsM3,4
#define		B1M3M	FlagsM3,5
#define		B2M3M	FlagsM3,6
			
FlagsM4			equ	0x15C
#define		B1M4A	FlagsM4,1
#define		B2M4A	FlagsM4,2
#define		B1M4G	FlagsM4,3
#define		B2M4G	FlagsM4,4
#define		B1M4M	FlagsM4,5
#define		B2M4M	FlagsM4,6
			
FlagsM5			equ	0x15D
#define		B1M5A	FlagsM5,1
#define		B2M5A	FlagsM5,2
#define		B1M5G	FlagsM5,3
#define		B2M5G	FlagsM5,4
#define		B1M5M	FlagsM5,5
#define		B2M5M	FlagsM5,6
			
FlagsM6			equ	0x15E
#define		B1M6A	FlagsM6,1
#define		B2M6A	FlagsM6,2
#define		B1M6G	FlagsM6,3
#define		B2M6G	FlagsM6,4
#define		B1M6M	FlagsM6,5
#define		B2M6M	FlagsM6,6

DMGAH		equ	0x15F
DMGAL		equ	0x160		

DMGGH		equ	0x161
DMGGL		equ	0x162

DMGMH		equ	0x163
DMGML		equ	0x164	

CheckDir	equ	0x165		
;FlagsCS			equ	0x15F
;#define		S1C0	FlagsCS,1
;#define		S2C0	FlagsCS,2
;#define		S1C1	FlagsCS,3
;#define		S2C1	FlagsCS,4
;#define		S1C2	FlagsCS,5
;#define		S2C2	FlagsCS,6
;			
;;FlagsCR			equ	0x160
;;#define		RC0	FlagsCR,1
;;#define		RC1	FlagsCR,2
;;#define		RC2	FlagsCR,3			
;			
;FlagsE1S		equ	0x161
;#define		S1E0	FlagsE1S,1
;#define		S2E0	FlagsE1S,2
;#define		S1E1	FlagsE1S,3
;#define		S2E1	FlagsE1S,4
;#define		S1E2	FlagsE1S,5
;#define		S2E2	FlagsE1S,6
;;			
;;FlagsE2S		equ	0x162
;;#define		S1E3	FlagsE2S,1
;;#define		S2E3	FlagsE2S,2
;;#define		S1E4	FlagsE2S,3
;;#define		S2E4	FlagsE2S,4
;;#define		S1E5	FlagsE2S,5
;;#define		S2E5	FlagsE2S,6
;;		
;;;FlagsER			equ	0x163
;;;#define		RE0	FlagsER,1
;;;#define		RE1	FlagsER,2
;;;#define		RE2	FlagsER,3
;;;#define		RE3	FlagsER,4
;;;#define		RE4	FlagsER,5
;;;#define		RE5	FlagsER,6		
;;			
;FlagsM1S		equ	0x164
;#define		S1M1A	FlagsM1S,1
;#define		S2M1A	FlagsM1S,2
;#define		S1M1G	FlagsM1S,3
;#define		S2M1G	FlagsM1S,4
;#define		S1M1M	FlagsM1S,5
;#define		S2M1M	FlagsM1S,6
;;		
;FlagsM2S		equ	0x165
;#define		S1M2A	FlagsM2S,1
;#define		S2M2A	FlagsM2S,2
;#define		S1M2G	FlagsM2S,3
;#define		S2M2G	FlagsM2S,4
;#define		S1M2M	FlagsM2S,5
;#define		S2M2M	FlagsM2S,6
;;
;;;FlagsM12R		equ	0x166
;;;#define		RM1A	FlagsM12R,1
;;;#define		RM1G	FlagsM12R,2
;;;#define		RM1M	FlagsM12R,3
;;;#define		RM2A	FlagsM12R,4
;;;#define		RM2G	FlagsM12R,5
;;;#define		RM2M	FlagsM12R,6		
;;			
;FlagsM3S		equ	0x167
;#define		S1M3A	FlagsM3S,1
;#define		S2M3A	FlagsM3S,2
;#define		S1M3G	FlagsM3S,3
;#define		S2M3G	FlagsM3S,4
;#define		S1M3M	FlagsM3S,5
;#define		S2M3M	FlagsM3S,6
;			
;FlagsM4S		equ	0x168
;#define		S1M4A	FlagsM4S,1
;#define		S2M4A	FlagsM4S,2
;#define		S1M4G	FlagsM4S,3
;#define		S2M4G	FlagsM4S,4
;#define		S1M4M	FlagsM4S,5
;#define		S2M4M	FlagsM4S,6
;
;;FlagsM34R		equ	0x169
;;#define		RM3A	FlagsM34R,1
;;#define		RM3G	FlagsM34R,2
;;#define		RM3M	FlagsM34R,3
;;#define		RM4A	FlagsM34R,4
;;#define		RM4G	FlagsM34R,5
;;#define		RM4M	FlagsM34R,6		
;			
;FlagsM5S		equ	0x170
;#define		S1M5A	FlagsM5S,1
;#define		S2M5A	FlagsM5S,2
;#define		S1M5G	FlagsM5S,3
;#define		S2M5G	FlagsM5S,4
;#define		S1M5M	FlagsM5S,5
;#define		S2M5M	FlagsM5S,6
;			
;FlagsM6S		equ	0x171
;#define		S1M6A	FlagsM6S,1
;#define		S2M6A	FlagsM6S,2
;#define		S1M6G	FlagsM6S,3
;#define		S2M6G	FlagsM6S,4
;#define		S1M6M	FlagsM6S,5
;#define		S2M6M	FlagsM6S,6
			
;FlagsM56R		equ	0x172
;#define		RM5A	FlagsM56R,1
;#define		RM5G	FlagsM56R,2
;#define		RM5M	FlagsM56R,3
;#define		RM6A	FlagsM56R,4
;#define		RM6G	FlagsM56R,5
;#define		RM6M	FlagsM56R,6
		
BanderasRam		equ	0x172		
#define		SEND	BanderasRam,1
#define		ReboseR	BanderasRam,2
#define		S1	BanderasRam,3
#define		S2	BanderasRam,4

contR	equ 0x173
BanderaRebose	equ 0x174 ; 00 y 01 no hay nada, 02 primer bufer lleno, 03 segundo bufer lleno y rebose
Escrito		equ 0x175	
Verficar MACRO  DH,DL
	banksel	DH
	movf	DH,w,1
	btfsc	STATUS,Z,0
	incf	BanderaRebose
	btfsc	DH,0,1
	incf	BanderaRebose
	btfsc	DH,1,1
	incf	BanderaRebose
	ENDM
Aumento	MACRO	DH,DL
	banksel	DH
	infsnz	DL,1,1
	incf	DH,1,1
	ENDM
Escribe	MACRO	Dato
	movf	Dato,w,1
	movwf	POSTINC0,0
	ENDM
EscribeR MACRO	Dato
	movf	Dato,w,1
	movwf	POSTINC2,0
	incf	contR,1,1
	incf	Escrito
	ENDM
Reinicio MACRO	DH,DL
	banksel	DH
	movlw	0x04
	subwf	DH,w,1
	movwf	DH,1
	clrf	DL,1
	ENDM

	
;de la poscion 0x500 a 0x509 se almacena rebose de C0
;de la poscion 0x510 a 0x519 se almacena rebose de C1
;de la poscion 0x520 a 0x529 se almacena rebose de C2
;de la poscion 0x530 a 0x539 se almacena rebose de E0
;de la poscion 0x540 a 0x549 se almacena rebose de E1
;de la poscion 0x550 a 0x559 se almacena rebose de E2
;de la poscion 0x560 a 0x569 se almacena rebose de E3
;de la poscion 0x570 a 0x579 se almacena rebose de E4
;de la poscion 0x580 a 0x589 se almacena rebose de E5
;de la poscion 0x590 a 0x599 se almacena rebose de M1A
;de la poscion 0x600 a 0x609 se almacena rebose de M1G
;de la poscion 0x610 a 0x619 se almacena rebose de MIM
;de la poscion 0x620 a 0x629 se almacena rebose de M2A
;de la poscion 0x630 a 0x639 se almacena rebose de M2G
;de la poscion 0x640 a 0x649 se almacena rebose de M2M
;de la poscion 0x650 a 0x659 se almacena rebose de M3A
;de la poscion 0x660 a 0x669 se almacena rebose de M3G
;de la poscion 0x670 a 0x679 se almacena rebose de M3M
;de la poscion 0x680 a 0x689 se almacena rebose de M4A
;de la poscion 0x690 a 0x699 se almacena rebose de M4G
;de la poscion 0x700 a 0x709 se almacena rebose de M4M
;de la poscion 0x710 a 0x719 se almacena rebose de M5A
;de la poscion 0x720 a 0x729 se almacena rebose de M5G
;de la poscion 0x730 a 0x739 se almacena rebose de M5M
;de la poscion 0x740 a 0x749 se almacena rebose de M6A
;de la poscion 0x750 a 0x759 se almacena rebose de M6G
;de la poscion 0x760 a 0x769 se almacena rebose de M6M			
;
;**************************************************************************************************
;Macros
;**************************************************************************************************



;
;Buffer impar out EP1 (recibe datos via USB provenientes de la PC)
;received_dt1	equ 	0x5C0
;received_dt2	equ 	0x5C1
;received_dt3	equ 	0x5C2
;received_dt4	equ 	0x5C3
;received_dt5	equ 	0x5C4
;received_dt6	equ 	0x5C5
;received_dt7	equ 	0x5C6
;received_dt8	equ 	0x5C7
;received_dt9	equ 	0x5C8
;received_dt10	equ 	0x5C9
;received_dt11	equ 	0x5CA
;received_dt12	equ 	0x5CB
;received_dt13	equ 	0x5CC
;received_dt14	equ 	0x5CD
;received_dt15	equ 	0x5CE
;received_dt16	equ 	0x5CF
;received_dt17	equ 	0x5D0
;received_dt18	equ 	0x5D1
;received_dt19	equ 	0x5D2
;received_dt20	equ 	0x5D3
;received_dt21	equ 	0x5D4
;received_dt22	equ 	0x5D5
;received_dt23	equ 	0x5D6
;received_dt24	equ 	0x5D7
;received_dt25	equ 	0x5D8
;received_dt26	equ 	0x5D9
;received_dt27	equ 	0x5DA
;received_dt28	equ 	0x5DB
;received_dt29	equ 	0x5DC
;received_dt30	equ 	0x5DD
;received_dt31	equ 	0x5DE
;received_dt32	equ 	0x5DF
;
;
;
;
;------DEFINICIONES PARA TARJETA HIDEYO--------------------------------------------------------------
;SPI (PIC18F2550):
;SCLK (RB1)
;MISO (RB0)
;MOSI (RC7)
;Vpp=RE3
;RB7=PGD
;RB6=PGC
;14=Vusb
;RC4=D-
;RC5=D+
;RA6=Main clock
;PORTA=RA0-RA6 (disponibles RA0-RA5), RA5=/SS
;PORTB=RB0-RB7 (disponbiles RB2-RB5)
;PORTC=RC0-RC2 y RC4-RC7 (disponibles RC0-RC2,RC6)
;
;
;
;
;Para este programa:
;RA2 lee un switch para iniciar la Tx SPI
;RA5 se lee para saber si el esclavo SPI puede recibir datos (RA5=L)
;RA0 (LATA0) se usa para encender un LED para indicar TX en proceso
;
;
;
	org 	0x0000
	goto 	INICIO
;
	org 	0x0008				;Direcci�n llamada si hubo una interrupci�n
	goto 	ISR
;
;
;**************************************************************************************************
; Rutina principal del firmware.
;**************************************************************************************************
INICIO
WAIT_FOR_CLOCK
	btfss 	OSCCON, OSTS, 0
	goto 	WAIT_FOR_CLOCK

	call	INIT_VARS		;Inicializa las variables
	call	INIT_PORTS		;RA0-RA1,RA4,RA7=Outputs. RA2,RA3,RA5,RA6=In (RA6 usado por el reloj).
					;RB0(MISO),RB2=In;RB1(SCK),RB3-RB7=Out.
	
	banksel cont
	clrf	cont, 1
	clrf 	USB_REG, 1			;Se limpia el registro de estados del USB.
	clrf 	USB_REG2, 1			;Se limpia el registro 2 de estados del USB.
;	call 	USB_INIT, 0	
;
	call	INIT_SPI_Master
	banksel	LATA
	bsf	LATA,5, 1		;pone en alto /CS(Data logger)
	bsf	LATA,0, 1		;pone en alto CS(RAM)
;
;
Tecla
	banksel	PORTB
	btfsc	PORTB,2, 1
	goto	Tecla
	call    Delay100ms
	btfsc	PORTB,2, 1
	goto	Tecla
;
	banksel	LATA
	bsf	LATA,0, 1		;prende LED
	bsf	LATA,4, 1               ;activa se�al Recording
;
	call	Delay1s	
	call	Delay1s
	call	Delay1s
	call	Delay1s
	call	Delay1s
	banksel	Estados
	clrf	Estados, 1
	lfsr	FSR0,0x200		;apunta a inicio de primer buffer ( DE 200h a 3FFh)
	call	INIT_TMR1		;programa tmr1 para int. cada 2ms
;
;Direcciones de los bufferes a emplear:
; Buffer1 = 200h-3ffh
; Buffer2 = 400h-5FFh
; Buffer3 = 600h-7FFh
;**************************************************************************************************
; 
;**************************************************************************************************
;Programa principal, espera que las banderas TxBuff1 o TxBuff2 se ponga alguna en alto, para
;enviar el correspondiente contenido del buffer por SPI. O si la bandera Final se pone en alto 
;termina la ejecuci�n del programa
Main
	banksel	Banderas
	btfsc	TxBuff1, 1
	goto	TxSPIBuff1
;	btfsc	send, 1
;	goto	lectura
	btfsc	Final, 1
	goto	Acaba
	banksel	PORTB
	btfsc	PORTB,2, 1
	goto	Main
	call    Delay100ms
	banksel	PORTB
	btfsc	PORTB,2, 1
	goto	Main
	;goto	Acaba
;
TxSPIBuff1
	lfsr	FSR1,0x200		;apunta a inicio de primer buffer
CicTx1
	movf	POSTINC1,w, 0		;Byte del primer buffer
	call	SPI_WRITE
;
	movf	FSR1L,1, 0
	btfss	STATUS,Z, 0
	goto	CicTx1
	movlw	0x06
	subwf	FSR1H,w, 0
	btfss	STATUS,Z, 0
	goto	CicTx1
	banksel	Banderas
	bcf	TxBuff1, 1
	call	CuentaSends
	goto	Main
;
Acaba
	banksel	LATA
	bcf	LATA,0, 1		;apaga LED
	bcf	LATA,4, 1		;apaga se�al recording
	movlw 	b'00000000'
	movwf 	INTCON, 0		;Apaga interrupciones globales y de perif�ricos
	nop
	goto	Acaba
;	
;-------------------------------------------------------------------------------
;Se producen 768,000,000 Bytes en 10h. Esto arroja un total de 750,000 (0x0B71B0) archivos de 1024B
;En 10 min se produce 12,800,000.Esto arroja un total de 12,500 (0x0030D4) archivos de 1024
;En 1 min se produce 1,280,000.Esto arroja un total de 1,250 (0x0004E2) archivos de 1024B
;En 1 hr se produce 76,800,000.Esto arroja un total de 75,000 (0x0124F8) archivos de 1024B
;En 2 hr se produce 153,600,00.Esto arroja un total de 150,000 (0x0249F0) archivos de 1024B	
;En 3 hr se produce 230,400,000.Esto arroja un total de 225,000 (0x036EE8) archivos de 1024B	
CuentaSends
	banksel	Cuenta
	incfsz	Cuenta,1, 1
	goto	ValSends
	incfsz	Cuenta1,1, 1
	goto	ValSends
	incf	Cuenta2,1, 1
ValSends
	movlw	0xE2
	subwf	Cuenta,w, 1
	btfss	STATUS,Z, 0
	return
	movlw	0x04
	subwf	Cuenta1,w, 1
	btfss	STATUS,Z, 0
	return
	movlw	0x00	
	subwf	Cuenta2,w, 1
	btfss	STATUS,Z, 0
	return
	banksel	Banderas
	bsf	Final, 1
	return
;**************************************************************************************************	
;
;
;
;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;lectura
;	btfsc	FlagsC,B1c0,1
;	goto	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
;canalC0mitad
;	lfsr	fsr1,0x600

	
;	return
;;USB puede intercambiar 64B(0x40) por transferencia. 
;;Como en este programa se manejan los datos de los buffers ping-pong
;;(par-impar se manejan llenando cada buffer con 64B (40h) de manera alterna:	
;;As� el buffer par ping-pong va de 600h-63Fh o para el buffer impar ping-pong de 640h-67Fh
;;y entonces enviarlos por USB. 
;CheckNoDatos
;	movf	FSR2L,w, 0
;	banksel USB_REG3
;	btfsc 	USB_REG3,EP1_IN_PPBI, 1 	; Verifica si la �ltima transferencia USB fue par(0).
;	sublw 	0x40 				; Verifica si lleg� al �ltimo byte impar del buffer USB (3F+1).
;	btfss 	USB_REG3,EP1_IN_PPBI, 1 	; Verifica si la �ltima transferencia USB fue impar(1).
;	sublw 	0x80				; Verifica si lleg� al �ltimo byte par del buffer USB (7F+1).
;	btfsc	STATUS,Z, 0
;	call 	SEND_RECEIVED_DATA_USB
;	return
;;
;;
;;**************************************************************************************************
;; Rutina para enviar datos a la PC v�a USB.
;;**************************************************************************************************
;SEND_RECEIVED_DATA_USB
;	call 	SEND_EP1_DATA		; Llama a la rutina para configurar el SIE para que maneje el EP1 OUT.
;	call 	RESTART_FSRn 		; Se reinicia el apuntador FSRn.
;	return
;;	
;;
;RESTART_FSRn
;	movlw 	0x06 				; Apunta FSR2 a direcci�n 0x600 o 0x640,direcci�n inicial del buffer IN del USB
;						; par o impar.
;	movwf 	FSR2H, 0
;	banksel USB_REG3
;	btfsc 	USB_REG3, EP1_IN_PPBI, 1 	; Verifica si la �ltima transferencia USB fue par(0).
;	movlw 	0x00
;	btfss 	USB_REG3, EP1_IN_PPBI, 1 	; Verifica si la �ltima transferencia USB fue impar(1).
;	movlw 	0x40	
;	movwf 	FSR2L, 0
;	return
;;	
;;***********************************************************************************************************	
;;
;
;--------------------------------------------------------------------------------------------------
;*******************************************************************
	
;	movlw	0xCF
;	banksel	ACCaLo
;	movwf	ACCaLo, 1
;	movlw	0xFF
;	movwf	ACCaMd, 1
;	movlw	0xFF
;	movwf	ACCaHi, 1
;	movlw	0x20
;	movwf	ACCaVH, 1
;;
;	movlw	0xC0
;	banksel	ACCbLo
;	movwf	ACCbLo, 1
;	movlw	0xFF
;	movwf	ACCbMd, 1
;	movlw	0xFF
;	movwf	ACCbHi, 1
;	movlw	0x20
;	movwf	ACCbVH, 1
;
TestBuffer
	movf	FSR0L,w, 0
	banksel	ACCaLo
	movwf	ACCaLo, 1
	movf	FSR0H,w, 0
	banksel	ACCaMd
	movwf	ACCaMd, 1
	clrf	ACCaHi, 1
	clrf	ACCaVH, 1
;
	banksel	ACCbLo
	clrf	ACCbLo, 1
;	movlw	0x01
;	subwf	CualBuffer,w
;	btfsc	STATUS,Z
;	goto	EsBuf1
;	movlw	0x02
;	subwf	CualBuffer,w
;	btfsc	STATUS,z
;	goto	EsBuf2
;	movlw	0x08
;	goto	Restalos
;EsBuf1
;	movlw	0x04
;	goto	Restalos
;EsBuf2
	movlw	0x06
	;goto	Restalos
Restalos
	banksel	ACCbMd
	movwf	ACCbMd, 1
	clrf	ACCbHi, 1
	clrf	ACCbVH, 1
;
	call	Resta32b
;
	banksel	ACCbLo
	movf	ACCbLo,1, 1
	btfss	STATUS,Z, 0
	goto	NoEsCero
	movf	ACCbMd,1, 1
	btfss	STATUS,Z, 0
	goto	NoEsCero
	movf	ACCbHi,1, 1
	btfss	STATUS,Z, 0
	goto	NoEsCero
	movf	ACCbVH,1, 1
	btfss	STATUS,Z, 0
	goto	NoEsCero
;Si llega a este punto FSR0= 0x400,0x600 o 0x800. Se llen� el buffer
Lleno
;	banksel	CualBuffer
;	movlw	.1
;	subwf	CualBuffer,w, 1
;	btfsc	STATUS,Z, 0
;	goto	EsBuf1
;	movlw	.2
;	subwf	CualBuffer,w, 1
;	btfsc	STATUS,Z, 0
;	goto	EsBuf2
	bsf	TxBuff1, 1	;pone en alto bandera para enviar por SPI Buffer1
	return
;EsEl2
;	bsf	TxBuff2, 1	;pone en alto bandera para enviar por SPI Buffer2
;	return
;
;Se checa si el contenido de FSR0<0x400
NoEsCero
	banksel	ACCbVH
	btfss	ACCbVH,7, 1
	return			;FSR0<0x0600
	banksel	Banderas
	bsf	Rebose, 1
	goto	Lleno		;FSR0>0x400 o a 0x0600
;**************************************************************************************************
;
;
;
;
;Inicia rutina para restar ACCbVH:ACCbHi:ACCbMd:ACCbLo - ACCaVH:ACCaHi:ACCaMd:ACCaLo = ACCbVH:ACCbHi:ACCbMd:ACCbLo	
Resta32b
	banksel	ACCaLo
	comf    ACCaLo,1, 1
        comf	ACCaMd,1, 1
        comf	ACCaHi,1, 1
        comf	ACCaVH,1, 1
	incf	ACCaLo,1, 1
	btfsc	STATUS,Z, 0
        incf	ACCaMd,1, 1
        btfsc	STATUS,Z, 1
        incf	ACCaHi,1, 1
	btfsc	STATUS,Z, 0
	incf	ACCaVH,1, 1
	call	Suma32b
	return
;
;Inicia rutina para sumar ACCbVH:ACCbHi:ACCbMd:ACCbLo + ACCaVH:ACCaHi:ACCaMd:ACCaLo = ACCbVH:ACCbHi:ACCbMd:ACCbLo
Suma32b
  	banksel ACCaLo
	movf    ACCaLo,w, 1
	addwf   ACCbLo,1, 1
	movf    ACCaMd,w, 1
	addwfc  ACCbMd,1, 1
 	movf    ACCaHi,w, 1
	addwfc  ACCbHi,1, 1
	movf    ACCaVH,w, 1
	addwfc  ACCbVH,1, 1 	
	return
;********************************************************************
;--------------------------------------------------------------------------------------------------
;
;	
;	
; Configura e inicializa los puertos.
INIT_PORTS		
	;------PORTA------
	clrf	PORTA, 0
	clrf	LATA, 0
	movlw	0x0F
	iorwf	ADCON1, 1, 0		;Configura todos los pines como digitales utilizando una m�scara OR inclusiva.
	movlw 	0x07
	movwf 	CMCON, 0 		;Deshabilita los comparadores.
	movlw	b'01001100'		;RA0-RA1,RA4 como salidas,RA2,RA3,RA5,RA6=In (por el reloj),RA7=Out.
	movwf	TRISA, 0		
	;------PORTB------
	clrf	PORTB, 0
	clrf	LATB, 0
	movlw	b'00000101'		;RB0(MISO),RB2=In;RB1(SCK),RB3-RB7=Out,
	movwf	TRISB, 0 		;
	clrf INTCON2,0
	bsf	INTCON2, 7, 1		;Deshabilita pull-ups del puerto B.
	;------PORTC------
	clrf	PORTC, 0
	clrf	LATC, 0
	movlw	b'00000010'		;RC1=In,RC7=Out(MOSI),RC4=D-,RC5=D+
	movwf	TRISC, 0		
	return

;**************************************************************************************************
; Inicializa la comunicaci�n SPI utilizando el m�dulo Master Synchronous Serial Port (MSSP).
;**************************************************************************************************
INIT_SPI_Master
	clrf	SSPCON1,0
	bsf	SSPSTAT,6, 0		;CKE=1, Tx on active to idle clock state
	movlw	b'00100000'		;SPI Master mode, clock=FOSC/4=48/4=12MHz,ClkIdle=L
	movwf	SSPCON1,0
	return

;**************************************************************************************************
; Inicializa las variables necesarias para la ejecuci�n del firmware.
;**************************************************************************************************
	
;**************************************************************************************************
; Inicializa la comunicaci�n SPI en modo esclavoutilizando el m�dulo 
; Master Synchronous Serial Port (MSSP).
;**************************************************************************************************
INIT_SPI_Slave
	clrf	SSPCON1,0
	bsf	SSPSTAT,6, 0		;CKE=1, Tx on active to idle clock state
	movlw	b'00100100'		;SPI Slve mode, /ss activo
	movwf	SSPCON1,0
	return

;**************************************************************************************************
; Inicializa las variables necesarias para la ejecuci�n del firmware.
;**************************************************************************************************
INIT_VARS
	banksel DC0H
	movlw   0x00
	movwf   DC0H, 1
	movlw   0x00
	movwf   DC0L, 1
	
	movlw   0x04
	movwf   DC1H, 1
	movlw   0x00
	movwf   DC1L, 1
	
	movlw   0x08
	movwf   DC2H, 1
	movlw   0x00
	movwf   DC2L, 1
		
	movlw   0x0C
	movwf   DE0H, 1
	movlw   0x00
	movwf   DE0L, 1
			
	movlw   0x10 
	movwf   DE1H, 1
	movlw   0x00
	movwf   DE1L, 1
			
	movlw   0x14
	movwf   DE2H, 1
	movlw   0x00
	movwf   DE2L, 1
			
	movlw   0x18
	movwf   DE3H, 1
	movlw   0x00
	movwf   DE3L, 1
			
	movlw   0x1C
	movwf   DE4H, 1
	movlw   0x00
	movwf   DE4L, 1
			
	movlw   0x20
	movwf   DE5H, 1
	movlw   0x00
	movwf   DE5L, 1
			
	movlw   0x24
	movwf   DM1AH, 1
	movlw   0x00
	movwf   DM1AL, 1
				
	movlw   0x28
	movwf   DM1GH, 1
	movlw   0x00
	movwf   DM1GL, 1
			
	movlw   0x2C
	movwf   DM1MH, 1
	movlw   0x00
	movwf   DM1AL, 1
				
	movlw   0x30
	movwf   DM2AH, 1
	movlw   0x00
	movwf   DM2AL, 1
				
	movlw   0x34
	movwf   DM2GH, 1
	movlw   0x00
	movwf   DM2GL, 1
				
	movlw   0x38
	movwf   DM2MH, 1
	movlw   0x00
	movwf   DM2ML, 1
					
	movlw   0x3C
	movwf   DM3AH, 1
	movlw   0x00
	movwf   DM3AL, 1
						
	movlw   0x40
	movwf   DM3GH, 1
	movlw   0x00
	movwf   DM3GL, 1
						
	movlw   0x44
	movwf   DM3MH, 1
	movlw   0x00
	movwf   DM3ML, 1
						
	movlw   0x48
	movwf   DM4AH, 1
	movlw   0x00
	movwf   DM4AL, 1
						
	movlw   0x4C
	movwf   DM4GH, 1
	movlw   0x00
	movwf   DM4GL, 1
						
	movlw   0x50
	movwf   DM4MH, 1
	movlw   0x00
	movwf   DM4ML, 1
						
	movlw   0x54
	movwf   DM5AH, 1
	movlw   0x00
	movwf   DM5AL, 1
						
	movlw   0x58
	movwf   DM5GH, 1
	movlw   0x00
	movwf   DM5GL, 1
						
	movlw   0x5C
	movwf   DM5MH, 1
	movlw   0x00
	movwf   DM5ML, 1
						
	movlw   0x60
	movwf   DM6AH, 1
	movlw   0x00
	movwf   DM6AL, 1
						
	movlw   0x64
	movwf   DM6GH, 1
	movlw   0x00
	movwf   DM6GL, 1
						
	movlw   0x68
	movwf   DM6MH, 1
	movlw   0x00
	movwf   DM6ML, 1
	
	;movlw	20h
	;movwf	FSR0H, 0
	;clrf	FSR0L, 0		;El FSR0 apunta al inicio de la memoria lineal
	;movlw	20h
	;movwf	FSR1H, 0
	;clrf	FSR1L,0 		;El FSR1 apunta al inicio de la memoria lineal
;	banksel	Menu
;	clrf	Menu, 1
;	banksel cont_tmr1
;	clrf 	cont_tmr1, 1
;	clrf 	cont_seg, 1
;	banksel TRNF_REG
;	clrf	TRNF_REG, 1
;	banksel USB_REG3
;	clrf	USB_REG3, 1
;	bsf 	USB_REG3, EP1_IN_PPBI, 1
;
	LFSR	FSR0,0x080
CicLimpia
	clrf	POSTINC0
	movlw	0x0D
	subwf	FSR0L,w ,0
	btfss	STATUS,Z, 0
	goto	CicLimpia
	movlw	0x01
	subwf	FSR0H,w ,0
	btfss	STATUS,Z, 0
	goto	CicLimpia
	banksel	Banderas
	clrf	Banderas
	banksel Cuenta
	clrf	Cuenta, 1
	clrf	Cuenta1, 1
	clrf	Cuenta2, 1
	return
	
	
;	banksel	Columna_H
;	clrf	Columna_H
;	clrf	Columna_L	
;	clrf	Pagina		    
;	clrf	Block_H
;	clrf	Block_L	
;;
;	banksel	TmpLo
;	clrf	TmpLo, 1
;	clrf	TmpMd, 1
;	clrf	TmpHi, 1
;	clrf	TmpVH, 1
;;
;	return
;**************************************************************************************************
;
;
;**************************************************************************************************
; Envia datos por medio de SPI.
;**************************************************************************************************
SPI_WRITE
	banksel	LATA
	btfsc	BanderasRam,SEND
	bcf	LATA,0, 1		;pone en bajo /CS
	bcf	LATA,5, 1
	nop
	movwf	SSPBUF, 0
EsperaSSPIF
	btfss	PIR1,SSPIF, 0			;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	goto	EsperaSSPIF			;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	bcf	PIR1,SSPIF, 0
	movf	SSPBUF,w, 0
	banksel BUFI
	movwf	BUFI, 1
;
	nop
	banksel	LATA
	bsf	LATA,0, 1
	bsf	LATA,5, 1		;pone en alto /CS
	return

;**************************************************************************************************
;
;---------------------------------------Funciones USB----------------------------------------------
;	
;**************************************************************************************************
;Inicializacion del modulo USB.
;**************************************************************************************************
;USB_INIT
;	clrf 	UCON, 0
;	movlw 	b'00010111'
;	movwf 	UCFG, 0
;	clrf 	UIR, 0				;Se limpia las banderas de interrupciones del USB.
;	clrf 	UEIR, 0				;Se limpia las banderas de interrupciones por error del USB.
;	movlw 	b'00000001'
;	movwf 	UIE, 0
;	setf 	UEIE, 0
;	movlw 	b'00010110'
;	movwf 	UEP0, 0			;EP0 habilitado para transferencias de control (IN, OUT y SETUP).
;	clrf	UEP1, 0			;EP1 deshabilitado hasta que el dispositivo USB sea enumerado.
;
;	banksel BD0STAT			
;	movlw 	b'10001000'
;	movwf 	BD0STAT, 1		;BD0 es OUT del EP0 (En modo sin Ping-Pong). Habilitado.
;	movlw 	.64
;	movwf 	BD0CNT, 1		;N�mero total de bytes transmitidos/recibidos en el endpoint.
;	movlw 	0x05
;	movwf 	BD0ADRH, 1		;Parte alta de la direcci�n inicial que corresponde a los datos del BD.
;	clrf 	BD0ADRL, 1		;Parte baja...
;	
;	clrf	BD1STAT, 1		;BD1 es IN del EP0 (En modo sin Ping-Pong). Deshabilitado.
;	movlw 	.64
;	movwf 	BD1CNT, 1		
;	movlw 	0x05
;	movwf 	BD1ADRH, 1		
;	movlw	0x40
;	movwf 	BD1ADRL, 1		
;	
;	clrf 	BD2STAT, 1		;BD2 es OUT (buffer par) del EP1 (con Ping-Pong). Deshabilitado.
;	movlw 	.64
;	movwf 	BD2CNT, 1		
;	movlw 	0x05
;	movwf 	BD2ADRH, 1		
;	movlw	0x80
;	movwf 	BD2ADRL, 1		
;	
;	clrf	BD3STAT, 1		;BD3 es OUT (buffer impar) del EP1 (con Ping-Pong). Deshabilitado.
;	movlw 	.64
;	movwf 	BD3CNT, 1		
;	movlw 	0x05
;	movwf 	BD3ADRH, 1		
;	movlw	0xC0
;	movwf 	BD3ADRL, 1		
;	
;	clrf 	BD4STAT, 1		;BD4 es IN (buffer par) del EP1 (con Ping-Pong). Deshabilitado.
;	movlw 	.64
;	movwf 	BD4CNT, 1		
;	movlw 	0x06
;	movwf 	BD4ADRH, 1		
;	movlw	0x00
;	movwf 	BD4ADRL, 1		
;	
;	clrf	BD5STAT, 1		;BD5 es IN (buffer impar) del EP1 (con Ping-Pong). Deshabilitado.
;	movlw 	.64
;	movwf 	BD5CNT, 1		
;	movlw 	0x06
;	movwf 	BD5ADRH, 1		
;	movlw	0x40
;	movwf 	BD5ADRL, 1	
;	
;	clrf 	UADDR, 0		;La direcci�n del dispositivo USB se hace cero, lo que significa que no ha sido enumerado.
;	call	USB_ON, 0		;Enciende el m�dulo USB.
;	
;	;********************Configura las interrupciones***********************
;	bsf 	RCON, IPEN, 0		;Habilita las prioridades en interrupciones.
;	bsf 	IPR2, USBIP, 0		;Interrupciones del USB son de alta prioridad.
;	bcf 	PIR2, USBIF, 0		;Limpia la bandera de interrupcion de USB.
;	bsf 	PIE2, USBIE, 0		;Habilita interrupci�n del USB.
;	movlw 	b'00000001'
;	movwf 	UIE, 0				;hab. int. por reset (ya se hizo arriba)
;	movlw 	b'10000000'
;	movwf 	INTCON, 0			;Solo habilita las interrupciones de alta prioridad.
;WAIT_ENUMERATION
;	banksel USB_REG
;	btfss 	USB_REG, CONF, 1	;Espera que el dispositivo se enumere.
;	goto 	WAIT_ENUMERATION
;	return
;
;;**************************************************************************************************
;; Enciende el m�dulo USB y espera que la condici�n SE0 termine.
;;**************************************************************************************************
;USB_ON
;	bsf	UCON,USBEN, 0		;Se enciende el m�dulo USB.
;	banksel USB_REG
;	bsf	USB_REG,ATTA, 1		;El USB se encuentra en estado ATTACHED.
;WAIT_SE0	
;	btfsc	UCON,SE0, 0		;espera SingleEnded0 flag=L
;	goto 	WAIT_SE0
;	bsf	USB_REG,POW, 1		;El USB se encuentra en estado POWERED.
;	return
;
;**************************************************************************************************
; Servicio principal de interrupciones para verificar que tipo de interrupci�n ha ocurrido. Se
; utiliza para establecer la comunicaci�n USB.
;**************************************************************************************************
ISR
	;banksel W_TEMP
	movwf 	W_TEMP 				; W_TEMP is in virtual bank
	movff	STATUS, STATUS_TEMP		; STATUS_TEMP located anywhere
	movff 	BSR, BSR_TEMP			; BSR_TMEP located anywhere
	call 	ISR1
	;banksel BSR_TEMP
	movff 	BSR_TEMP, BSR			; Restore BSR
	movf 	W_TEMP, W 			; Restore WREG
	movff 	STATUS_TEMP, STATUS		; Restore STATUS
	retfie
;
ISR1
	btfsc 	PIR1, TMR1IF, 0 	; Checa si la interrupci�n fue por desborde del timer 1.
	call 	CHECK_TMR1
;	banksel USB_REG
;	btfss 	USB_REG, ATTA, 1	; Verifica si el dispositivo USB se encuentra en estado ATTACHED.
	return
;ACT_ISR_CHECK
;	btfss	UIE, ACTVIE, 0		;Verifica si la interrupci�n por actividad del bus USB est� habilitada.
;	goto	SUSPND_CHECK
;	btfsc	UIR, ACTVIF, 0		;Verifica si se levanto la bandera de interrupci�n por actividad del bus USB.
;	goto	ACTIVITY_ISR
;SUSPND_CHECK
;	btfsc	UCON,SUSPND, 0		;Verifica si el dispositivo USB se encuentra en estado SUSPENDED.
;	return
;RESET_ISR_CHECK
;	btfss	UIE, URSTIE, 0		;Verifica si la interrupci�n por reset del USB est� habilitada.
;	goto	ERR_ISR_CHECK
;	btfsc	UIR, URSTIF, 0		;Verifica si se levanto la bandera de interrupci�n por reset del USB.
;	goto	RESET_ISR
;ERR_ISR_CHECK
;	btfss	UIE, UERRIE, 0		;Verifica si la interrupci�n por error del USB est� habilitada.
;	goto	IDLE_ISR_CHECK
;	btfsc	UIR, UERRIF, 0		;Verifica si se levanto la bandera de interrupci�n por error del USB.
;	goto	ERROR_ISR
;IDLE_ISR_CHECK
;	btfss	UIE,IDLEIE, 0		;Verifica si la interrupci�n por deteccion de IDLE est� habilitada.
;	goto	STALL_ISR_CHECK
;	btfsc	UIR,IDLEIF, 0		;Verifica si se levanto la bandera de interrupci�n por detecci�n de IDLE.
;	goto	IDLE_ISR
;STALL_ISR_CHECK
;	btfss	UIE, STALLIE, 0		;Verifica si la interrupci�n por STALL del USB est� habilitada.
;	goto	SOF_ISR_CHECK
;	btfsc	UIR, STALLIF, 0		;Verifica si se levanto la bandera de interrupci�n por STALL del USB.
;	goto	STALL_ISR
;SOF_ISR_CHECK
;	btfss	UIE, SOFIE, 0		;Verifica si la interrupci�n por SOF del USB est� habilitada.
;	goto	DEF_STATE_CHECK
;	btfsc	UIR, SOFIF, 0		;Verifica si se levanto la bandera de interrupci�n por SOF del USB.
;	goto	SOF_ISR
;DEF_STATE_CHECK
;	banksel USB_REG
;	btfss 	USB_REG, DEF, 1		;Solo se procesan las interrupciones por transacci�n completa si el
;	return						;dispositivo se encuentra en estado DEFAULT o mayor.
;TRN_ISR_CHECK
;	btfss	UIE, TRNIE, 0		;Verifica si la interrupci�n por transacci�n completa est� habilitada.
;	goto	END_ISR
;	btfsc	UIR, TRNIF, 0		;Verifica si se levanto la bandera de interrupci�n por transacci�n completa.
;	goto	TRANS_ISR
;END_ISR
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1			;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1			;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return

CHECK_TMR1
	bcf 	PIR1, TMR1IF, 0
	movlw 	0xF4				;0x0B		
	movwf 	TMR1H, 0			; Se mueve el valor de la parte alta del timer 1.
	movlw 	0x47				;0xB8			
	movwf 	TMR1L, 0
	;call 	INIT_TMR1
	banksel	Estados
	incf	Estados,1, 1
;
	movlw	0x01
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado1
	movlw	0x02
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado2
	movlw	0x03
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado3
	movlw	0x04
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado4
	movlw	0x05
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado5
	movlw	0x06
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado6
	movlw	0x07
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado7
	movlw	0x08
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado8	
	movlw	0x09
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado9
	movlw	0x0A
	subwf	Estados,w, 1
	btfsc	STATUS,Z, 0
	goto	Estado10
	return
;
;*********************************************************************************************
Estado1	
CanalC0	
	lfsr	FSR2,0x600
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DC0H
	movf	DC0H,w,1
	movwf	POSTINC0, 0
	movf	DC0L,w,1
	movwf	POSTINC0, 0
;	
	banksel	C0emgLo
;	
	Verficar   DC0H,DC0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0emgLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0emgLo
	Aumento	   DC0H,DC0L
;	
	
	Verficar   DC0H,DC0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0emgMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0emgMd
	Aumento	   DC0H,DC0L
;	
	Verficar   DC0H,DC0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0emgHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0emgHi
	Aumento	   DC0H,DC0L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1C0,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalC1
	bsf	   B2C0,1
;	
	Reinicio   DC0H,DC0L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DC0H
	movf	DC0H,w,1
	movwf	POSTINC0, 0
	movf	DC0L,w,1
	movwf	POSTINC0, 0
DatosC0	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DC0L,1,1
	decfsz	contR,1,1
	BRA	DatosC0
	clrf	BanderaRebose
	call	EscribeRam
CanalC1
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DC1H	
	movf	DC1H,w,1
	movwf	POSTINC0, 0
	movf	DC1L,w,1
	movwf	POSTINC0, 0
	banksel	C1emgLo
;	
	Verficar   DC1H,DC1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1emgLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1emgLo
	Aumento	   DC1H,DC1L
;	
	Verficar   DC1H,DC1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1emgMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1emgMd
	Aumento	   DC1H,DC1L
;	
	Verficar   DC1H,DC1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1emgHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1emgHi
	Aumento	   DC1H,DC1L
	call	   EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1C1,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalC2
	bsf	   B2C1,1
;	
	Reinicio   DC1H,DC1L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DC1H
	movf	DC1H,w,1
	movwf	POSTINC0, 0
	movf	DC1L,w,1
	movwf	POSTINC0, 0
DatosC1	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DC1L,1,1
	decfsz	contR,1,1
	BRA	DatosC1
	clrf	BanderaRebose
	call	EscribeRam	
;
CanalC2
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DC2H
	movf	DC2H,w,1
	movwf	POSTINC0, 0
	movf	DC2L,w,1
	movwf	POSTINC0, 0
	banksel	C2emgLo	
;	
	Verficar   DC2H,DC2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2emgLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C2emgLo
	Aumento	   DC2H,DC2L
;	
	Verficar   DC2H,DC2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2emgMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C2emgMd
	Aumento	   DC2H,DC2L
;	
	Verficar   DC2H,DC2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2emgHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Aumento	   DC2H,DC2L
	Escribe	   C2emgHi
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1C2,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   FinC
	bsf	   B2C2,1
;	
	Reinicio   DC2H,DC2L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DC2H
	movf	DC2H,w,1
	movwf	POSTINC0, 0
	movf	DC2L,w,1
	movwf	POSTINC0, 0
DatosC2	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DC2L,1,1
	decfsz	contR,1,1
	BRA	DatosC2
	clrf	BanderaRebose
	call	EscribeRam	
FinC
	clrf	BanderaRebose
	call	IncreEMG
	call	TestBuffer
	call	ValidaBuffer
	return			    ;hacia salida de int si se llama directamente
;*********************************************************************************************
;
;
;
;*********************************************************************************************
Estado2
	call	Estado1
;
CanalE0	
	lfsr	FSR2,0x600
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE0H
	movf	DE0H,w,1
	movwf	POSTINC0, 0
	movf	DE0L,w,1
	movwf	POSTINC0, 0
;	
	banksel	C0eegLo
;	
	Verficar   DE0H,DE0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0eegLo
	Aumento	   DE0H,DE0L
;	
	Verficar   DE0H,DE0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0eegMd
	Aumento	   DE0H,DE0L
;	
	Verficar   DE0H,DE0L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C0eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C0eegHi
	Aumento	   DE0H,DE0L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E0,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalE1
	bsf	   B2E0,1
;	
	Reinicio   DE0H,DE0L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE0H
	movf	DE0H,w,1
	movwf	POSTINC0, 0
	movf	DE0L,w,1
	movwf	POSTINC0, 0
DatosE0	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE0L,1,1
	decfsz	contR,1,1
	BRA	DatosE0
	clrf	BanderaRebose
	call	EscribeRam
CanalE1
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE1H	
	movf	DE1H,w,1
	movwf	POSTINC0, 0
	movf	DE1L,w,1
	movwf	POSTINC0, 0
	banksel	C1eegLo
;	
	Verficar   DE1H,DE1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1eegLo
	Aumento	   DE1H,DE1L
;	
	Verficar   DE1H,DE1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1eegMd
	Aumento	   DE1H,DE1L
;	
	Verficar   DE1H,DE1L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C1eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C1eegHi
	Aumento	   DE1H,DE1L
	call	   EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E1,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalE2
	bsf	   B2E1,1
;	
	Reinicio   DC1H,DC1L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE1H
	movf	DE1H,w,1
	movwf	POSTINC0, 0
	movf	DE1L,w,1
	movwf	POSTINC0, 0
DatosE1	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE1L,1,1
	decfsz	contR,1,1
	BRA	DatosE1
	clrf	BanderaRebose
	call	EscribeRam	
;
CanalE2
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE2H
	movf	DE2H,w,1
	movwf	POSTINC0, 0
	movf	DE2L,w,1
	movwf	POSTINC0, 0
	banksel	C2eegLo	
;	
	Verficar   DE2H,DE2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C2eegLo
	Aumento	   DE2H,DE2L
;	
	Verficar   DE2H,DE2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C2eegMd
	Aumento	   DE2H,DE2L
;	
	Verficar   DE2H,DE2L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C2eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C2eegHi
	Aumento	   DE2H,DE2L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E2,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalE3
	bsf	   B2E2,1
;	
	Reinicio   DE2H,DE2L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE2H
	movf	DE2H,w,1
	movwf	POSTINC0, 0
	movf	DE2L,w,1
	movwf	POSTINC0, 0
DatosE2	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE2L,1,1
	decfsz	contR,1,1
	BRA	DatosE2	
	clrf	BanderaRebose
	call	EscribeRam
CanalE3
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE3H
	movf	DE3H,w,1
	movwf	POSTINC0, 0
	movf	DE3L,w,1
	movwf	POSTINC0, 0
	banksel	C3eegLo	
;	
	Verficar   DE3H,DE3L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C3eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C3eegLo
	Aumento	   DE3H,DE3L
;	
	Verficar   DE3H,DE3L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C3eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C3eegMd
	Aumento	   DE3H,DE3L
;	
	Verficar   DE3H,DE3L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C3eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C3eegHi
	Aumento	   DE3H,DE3L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E3,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalE4
	bsf	   B2E3,1
;	
	Reinicio   DE3H,DE3L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE3H
	movf	DE3H,w,1
	movwf	POSTINC0, 0
	movf	DE3L,w,1
	movwf	POSTINC0, 0
DatosE3	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE3L,1,1
	decfsz	contR,1,1
	BRA	DatosE3	
	clrf	BanderaRebose
	call	EscribeRam
CanalE4
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE4H
	movf	DE4H,w,1
	movwf	POSTINC0, 0
	movf	DE4L,w,1
	movwf	POSTINC0, 0
	banksel	C4eegLo	
;	
	Verficar   DE4H,DE4L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C4eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C4eegLo
	Aumento	   DE4H,DE4L
;	
	Verficar   DE4H,DE4L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C4eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C4eegMd
	Aumento	   DE4H,DE4L
;	
	Verficar   DE4H,DE4L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C4eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C4eegHi
	Aumento	   DE4H,DE4L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E4,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CanalE5
	bsf	   B2E4,1
;	
	Reinicio   DE4H,DE4L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE4H
	movf	DE4H,w,1
	movwf	POSTINC0, 0
	movf	DE4L,w,1
	movwf	POSTINC0, 0
DatosE4	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE4L,1,1
	decfsz	contR,1,1
	BRA	DatosE4	
	clrf	BanderaRebose
	call	EscribeRam
CanalE5
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DE5H
	movf	DE5H,w,1
	movwf	POSTINC0, 0
	movf	DE5L,w,1
	movwf	POSTINC0, 0
	banksel	C5eegLo	
;	
	Verficar   DE5H,DE5L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C5eegLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C5eegLo
	Aumento	   DE5H,DE5L
;	
	Verficar   DE5H,DE5L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C5eegMd
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C5eegMd
	Aumento	   DE5H,DE5L
;	
	Verficar   DE5H,DE5L
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   C5eegHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   C5eegHi
	Aumento	   DE5H,DE5L
	call	EscribeRam
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1E5,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   FinE
	bsf	   B2E5,1
;	
	Reinicio   DE5H,DE5L
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DE5H
	movf	DE5H,w,1
	movwf	POSTINC0, 0
	movf	DE5L,w,1
	movwf	POSTINC0, 0
DatosE5	
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DE5L,1,1
	decfsz	contR,1,1
	BRA	DatosE5	
	clrf	BanderaRebose
	call	EscribeRam
FinE
	clrf	BanderaRebose
	call	IncreEMG
	call	TestBuffer
	call	ValidaBuffer
	return			    ;hacia salida de int si se llama directamente
;*********************************************************************************************
;
;
;
;*********************************************************************************************
Estado3	
	call	Estado1
	return			    ;hacia salida de int.
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado4
	call	Estado2
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado5
	call	Estado1
;
Estado5_1
	banksel	TmpMd
	movlw	.6
	movwf	TmpMd, 1	    ;se repite el almacenamiento 6 veces (6 IMUs)

CicloIMUA
	call	DireccionIMU
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DMGAH
	movf	DMGAH,w,1
	movwf	POSTINC0, 0
	movf	DMGAL,w,1
	movwf	POSTINC0, 0
	banksel	M1AXLo
;	
	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXLo
	Aumento	   DMGAH,DMGAL
;
	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXHi
	Aumento	   DMGAH,DMGAL

	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYLo
	Aumento	   DMGAH,DMGAL
;
	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYHi
	Aumento	   DMGAH,DMGAL
;
	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZLo
	Aumento	   DMGAH,DMGAL
;
	Verficar   DMGAH,DMGAL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZHi
	Aumento	   DMGAH,DMGAL
	call	EscribeRam
;	call	SalvarContA
	call	FlagsA
	movlw	   0x04
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CicloIMUG
;	
	Reinicio   DMGAH,DMGAL
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DMGAH
	movf	DMGAH,w,1
	movwf	POSTINC0, 0
	movf	DMGAL,w,1
	movwf	POSTINC0, 0
DatosMGA
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DMGAL,1,1
	decfsz	contR,1,1
	BRA	DatosMGA	
	clrf	BanderaRebose
	call	EscribeRam
;	call	SalvarContA
CicloIMUG
;	call	DireccionIMUG
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DMGGH
	movf	DMGGH,w,1
	movwf	POSTINC0, 0
	movf	DMGGL,w,1
	movwf	POSTINC0, 0
	banksel	M1AXLo
;	
	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXLo
	Aumento	   DMGGH,DMGGL
;
	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXHi
	Aumento	   DMGGH,DMGGL

	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYLo
	Aumento	   DMGGH,DMGGL
;
	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYHi
	Aumento	   DMGGH,DMGGL
;
	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZLo
	Aumento	   DMGGH,DMGGL
;
	Verficar   DMGGH,DMGGL
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZHi
	Aumento	   DMGGH,DMGGL
	call	EscribeRam
;	call	SalvarContG
	call	FlagsG
	movlw	   0x04
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   CicloIMUM
;	
	Reinicio   DMGGH,DMGGL
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DMGGH
	movf	DMGGH,w,1
	movwf	POSTINC0, 0
	movf	DMGGL,w,1
	movwf	POSTINC0, 0
DatosMGG
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DMGGL,1,1
	decfsz	contR,1,1
	BRA	DatosMGG	
	clrf	BanderaRebose
	call	EscribeRam
;	call	SalvarContG
CicloIMUM
;	call	DireccionIMU
	movlw	0x02
	movwf	POSTINC0, 0
	banksel	DMGMH
	movf	DMGMH,w,1
	movwf	POSTINC0, 0
	movf	DMGML,w,1
	movwf	POSTINC0, 0
	banksel	M1AXLo
;	
	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXLo
	Aumento	   DMGMH,DMGML
;
	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AXHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AXHi
	Aumento	   DMGMH,DMGML

	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYLo
	Aumento	   DMGMH,DMGML
;
	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AYHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AYHi
	Aumento	   DMGMH,DMGML
;
	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZLo
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZLo
	Aumento	   DMGMH,DMGML
;
	Verficar   DMGMH,DMGML
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	EscribeR   M1AZHi
	movf	   Escrito,w,1
	btfsc	   STATUS,Z,0
	Escribe	   M1AZHi
	Aumento	   DMGMH,DMGML
	call	EscribeRam
	call	SalvarCont
	call	FlagsM
	movlw	   0x04
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	BRA	   FinIMU
;	
	Reinicio   DMGMH,DMGML
	movlw	0x02
	movwf	POSTINC0,0
	banksel	DMGMH
	movf	DMGMH,w,1
	movwf	POSTINC0, 0
	movf	DMGML,w,1
	movwf	POSTINC0, 0
DatosMGM
	movf	POSTINC2,w,1
	movwf	POSTINC0,0
	incf	DMGML,1,1
	decfsz	contR,1,1
	BRA	DatosMGA	
	clrf	BanderaRebose
	call	EscribeRam
	call	SalvarCont
FinIMU
	clrf	BanderaRebose
	banksel	TmpMd
	decfsz	TmpMd,1, 1
	goto	CicloIMUA	
;
	call	IncreIMU
	call	TestBuffer
	call	ValidaBuffer
	return			    ;hacia salida de int. si se llama directamente	
;*********************************************************************************************		
;
;
;
;*********************************************************************************************
Estado6
	call	Estado2
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado7
	call	Estado1
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado8
	call	Estado2
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado9
	call	Estado1
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;*********************************************************************************************
Estado10
	call	Estado2
	call	Estado5_1
	banksel	Estados
	clrf	Estados, 1
	return			    ;hacia salida de int,
;*********************************************************************************************
;
;
;	
;		
;///////////////////////////////////////////////////////////////////////////////	
EscribeRam
	lfsr	FSR1,0x200		;apunta a inicio de primer buffer
	lfsr	FSR0,0x200
Ram1
	movf	POSTINC1,w, 0		;Byte del primer buffer
	call	SPI_WRITE
	movlw	0x05
	subwf	FSR1L,W,0
	btfss	STATUS,Z,0
	BRA	Ram1
	lfsr	FSR0,0x200
	lfsr	FSR2,0x600
	return	
DireccionIMU
	movlw	0x06
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Direccion1 
	movlw	0x05
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Direccion2
	movlw	0x04
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Direccion3
	movlw	0x03
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Direccion4
	movlw	0x02
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Direccion5
	movf	DM6AH,w,1
	movwf	DMGAH,1
	movf	DM6AL,w,1
	movwf	DMGAL,1
	movf	DM6GH,w,1
	movwf	DMGGH,1
	movf	DM6GL,w,1
	movwf	DMGGL,1
	movf	DM6MH,w,1
	movwf	DMGMH,1
	movf	DM6ML,w,1
	movwf	DMGML,1
	return
Direccion5
	movf	DM5AH,w,1
	movwf	DMGAH,1
	movf	DM5AL,w,1
	movwf	DMGAL,1
	movf	DM5GH,w,1
	movwf	DMGGH,1
	movf	DM5GL,w,1
	movwf	DMGGL,1
	movf	DM5MH,w,1
	movwf	DMGMH,1
	movf	DM5ML,w,1
	movwf	DMGML,1
	return
Direccion4
	movf	DM4AH,w,1
	movwf	DMGAH,1
	movf	DM4AL,w,1
	movwf	DMGAL,1
	movf	DM4GH,w,1
	movwf	DMGGH,1
	movf	DM4GL,w,1
	movwf	DMGGL,1
	movf	DM4MH,w,1
	movwf	DMGMH,1
	movf	DM4ML,w,1
	movwf	DMGML,1
	return
Direccion3
	movf	DM3AH,w,1
	movwf	DMGAH,1
	movf	DM3AL,w,1
	movwf	DMGAL,1
	movf	DM3GH,w,1
	movwf	DMGGH,1
	movf	DM3GL,w,1
	movwf	DMGGL,1
	movf	DM3MH,w,1
	movwf	DMGMH,1
	movf	DM3ML,w,1
	movwf	DMGML,1
	return
Direccion2
	movf	DM2AH,w,1
	movwf	DMGAH,1
	movf	DM2AL,w,1
	movwf	DMGAL,1
	movf	DM2GH,w,1
	movwf	DMGGH,1
	movf	DM2GL,w,1
	movwf	DMGGL,1
	movf	DM2MH,w,1
	movwf	DMGMH,1
	movf	DM2ML,w,1
	movwf	DMGML,1
	return
Direccion1
	movf	DM1AH,w,1
	movwf	DMGAH,1
	movf	DM1AL,w,1
	movwf	DMGAL,1
	movf	DM1GH,w,1
	movwf	DMGGH,1
	movf	DM1GL,w,1
	movwf	DMGGL,1
	movf	DM1MH,w,1
	movwf	DMGMH,1
	movf	DM1ML,w,1
	movwf	DMGML,1
	return

SalvarCont
	movlw	0x06
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Save1 
	movlw	0x05
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Save2
	movlw	0x04
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Save3
	movlw	0x03
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Save4
	movlw	0x02
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	Save5
	movf	DMGAH,w,1
	movwf	DM6AH,1
	movf	DMGAL,w,1
	movwf	DM6AL,1
	movf	DMGGH,w,1
	movwf	DM6GH,1
	movf	DMGGL,w,1
	movwf	DM6GL,1
	movf	DMGMH,w,1
	movwf	DM6MH,1
	movf	DMGML,w,1
	movwf	DM6ML,1
	return
Save5
	movf	DMGAH,w,1
	movwf	DM5AH,1
	movf	DMGAL,w,1
	movwf	DM5AL,1
	movf	DMGGH,w,1
	movwf	DM5GH,1
	movf	DMGGL,w,1
	movwf	DM5GL,1
	movf	DMGMH,w,1
	movwf	DM5MH,1
	movf	DMGML,w,1
	movwf	DM5ML,1
	return
Save4
	movf	DMGAH,w,1
	movwf	DM4AH,1
	movf	DMGAL,w,1
	movwf	DM4AL,1
	movf	DMGGH,w,1
	movwf	DM4GH,1
	movf	DMGGL,w,1
	movwf	DM4GL,1
	movf	DMGMH,w,1
	movwf	DM4MH,1
	movf	DMGML,w,1
	movwf	DM4ML,1
	return
Save3
	movf	DMGAH,w,1
	movwf	DM3AH,1
	movf	DMGAL,w,1
	movwf	DM3AL,1
	movf	DMGGH,w,1
	movwf	DM3GH,1
	movf	DMGGL,w,1
	movwf	DM3GL,1
	movf	DMGMH,w,1
	movwf	DM3MH,1
	movf	DMGML,w,1
	movwf	DM3ML,1
	return
Save2
	movf	DMGAH,w,1
	movwf	DM2AH,1
	movf	DMGAL,w,1
	movwf	DM2AL,1
	movf	DMGGH,w,1
	movwf	DM2GH,1
	movf	DMGGL,w,1
	movwf	DM2GL,1
	movf	DMGMH,w,1
	movwf	DM2MH,1
	movf	DMGML,w,1
	movwf	DM2ML,1
	return
Save1
	movf	DMGAH,w,1
	movwf	DM1AH,1
	movf	DMGAL,w,1
	movwf	DM1AL,1
	movf	DMGGH,w,1
	movwf	DM1GH,1
	movf	DMGGL,w,1
	movwf	DM1GL,1
	movf	DMGMH,w,1
	movwf	DM1MH,1
	movf	DMGML,w,1
	movwf	DM1ML,1	
	return	
;
FlagsA
	movlw	0x06
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsA1 
	movlw	0x05
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsA2
	movlw	0x04
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsA3
	movlw	0x03
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsA4
	movlw	0x02
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsA5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M6A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M6A,1
	return
FlagsA5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M5A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M5A,1
	return
FlagsA4
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M4A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M4A,1
	return
FlagsA3
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M3A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M3A,1
	return
FlagsA2
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M2A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M2A,1
	return
FlagsA1
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M1A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M1A,1
	return	
;	
;DireccionIMUG
;	movlw	0x06
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionG1 
;	movlw	0x05
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionG2
;	movlw	0x04
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionG3
;	movlw	0x03
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionG4
;	movlw	0x02
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionG5
;	movf	DM6GH,w,1
;	movwf	DMGGH,1
;	movf	DM6GL,w,1
;	movwf	DMGGL,1
;	return
;DireccionG5
;	movf	DM5GH,w,1
;	movwf	DMGGH,1
;	movf	DM5GL,w,1
;	movwf	DMGGL,1
;	return
;DireccionG4
;	movf	DM4GH,w,1
;	movwf	DMGGH,1
;	movf	DM4GL,w,1
;	movwf	DMGGL,1
;	return
;DireccionG3
;	movf	DM3GH,w,1
;	movwf	DMGGH,1
;	movf	DM3GL,w,1
;	movwf	DMGGL,1
;	return
;DireccionG2
;	movf	DM2GH,w,1
;	movwf	DMGGH,1
;	movf	DM2GL,w,1
;	movwf	DMGGL,1
;	return
;DireccionG1
;	movf	DM1GH,w,1
;	movwf	DMGGH,1
;	movf	DM1GL,w,1
;	movwf	DMGGL,1
;	return
;
;SalvarContG
;	movlw	0x06
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveG1 
;	movlw	0x05
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveG2
;	movlw	0x04
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveG3
;	movlw	0x03
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveG4
;	movlw	0x02
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveG5
;	movf	DMGGH,w,1
;	movwf	DM6GH,1
;	movf	DMGGL,w,1
;	movwf	DM6GL,1
;	return
;SaveG5
;	movf	DMGGH,w,1
;	movwf	DM5GH,1
;	movf	DMGGL,w,1
;	movwf	DM5GL,1
;	return
;SaveG4
;	movf	DMGGH,w,1
;	movwf	DM4GH,1
;	movf	DMGGL,w,1
;	movwf	DM4GL,1
;	return
;SaveG3
;	movf	DMGGH,w,1
;	movwf	DM3GH,1
;	movf	DMGGL,w,1
;	movwf	DM3GL,1
;	return
;SaveG2
;	movf	DMGGH,w,1
;	movwf	DM2GH,1
;	movf	DMGGL,w,1
;	movwf	DM2GL,1
;	return
;SaveG1
;	movf	DMGGH,w,1
;	movwf	DM1GH,1
;	movf	DMGGL,w,1
;	movwf	DM1GL,1
;	return	
;
FlagsG
	movlw	0x06
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsG1 
	movlw	0x05
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsG2
	movlw	0x04
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsG3
	movlw	0x03
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsG4
	movlw	0x02
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagsG5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M6G,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M6G,1
	return
FlagsG5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M5G,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M5G,1
	return
FlagsG4
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M4A,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M4A,1
	return
FlagsG3
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M3G,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M3G,1
	return
FlagsG2
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M2G,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M2G,1
	return
FlagsG1
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M1G,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M1G,1
	return	
;DireccionIMUM
;	movlw	0x06
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionM1 
;	movlw	0x05
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionM2
;	movlw	0x04
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionM3
;	movlw	0x03
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionM4
;	movlw	0x02
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	DireccionM5
;	movf	DM6MH,w,1
;	movwf	DMGMH,1
;	movf	DM6ML,w,1
;	movwf	DMGML,1
;	return
;DireccionM5
;	movf	DM5MH,w,1
;	movwf	DMGMH,1
;	movf	DM5ML,w,1
;	movwf	DMGML,1
;	return
;DireccionM4
;	movf	DM4MH,w,1
;	movwf	DMGMH,1
;	movf	DM4ML,w,1
;	movwf	DMGML,1
;	return
;DireccionM3
;	movf	DM3MH,w,1
;	movwf	DMGMH,1
;	movf	DM3ML,w,1
;	movwf	DMGML,1
;	return
;DireccionM2
;	movf	DM2MH,w,1
;	movwf	DMGMH,1
;	movf	DM2ML,w,1
;	movwf	DMGML,1
;	return
;DireccionM1
;	movf	DM1MH,w,1
;	movwf	DMGMH,1
;	movf	DM1ML,w,1
;	movwf	DMGML,1
;	return
;
;SalvarContM
;	movlw	0x06
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveM1 
;	movlw	0x05
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveM2
;	movlw	0x04
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveM3
;	movlw	0x03
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveM4
;	movlw	0x02
;	subwf	TmpMd,w,1
;	btfsc	STATUS,Z,0
;	BRA	SaveM5
;	movf	DMGMH,w,1
;	movwf	DM6MH,1
;	movf	DMGML,w,1
;	movwf	DM6ML,1
;	return
;SaveM5
;	movf	DMGMH,w,1
;	movwf	DM5MH,1
;	movf	DMGML,w,1
;	movwf	DM5ML,1
;	return
;SaveM4
;	movf	DMGMH,w,1
;	movwf	DM4MH,1
;	movf	DMGML,w,1
;	movwf	DM4ML,1
;	return
;SaveM3
;	movf	DMGMH,w,1
;	movwf	DM3MH,1
;	movf	DMGML,w,1
;	movwf	DM3ML,1
;	return
;SaveM2
;	movf	DMGMH,w,1
;	movwf	DM2MH,1
;	movf	DMGML,w,1
;	movwf	DM2ML,1
;	return
;SaveM1
;	movf	DMGMH,w,1
;	movwf	DM1MH,1
;	movf	DMGML,w,1
;	movwf	DM1ML,1
;	return	
;
FlagsM
	movlw	0x06
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagssM1 
	movlw	0x05
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagssM2
	movlw	0x04
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagssM3
	movlw	0x03
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagssM4
	movlw	0x02
	subwf	TmpMd,w,1
	btfsc	STATUS,Z,0
	BRA	FlagssM5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M6M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M6M,1
	return
FlagssM5
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M5M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M5M,1
	return
FlagssM4
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M4M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M4M,1
	return
FlagssM3
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M3M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M3M,1
	return
FlagssM2
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M2M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M2M,1
	return
FlagssM1
	movlw	   0x02
	subwf	   BanderaRebose,w,1
	btfsc	   STATUS,Z,0
	bsf	   B1M1M,1
	movlw	   0x03
	subwf	   BanderaRebose,w,1
	btfss	   STATUS,Z,0
	incf	   BanderaRebose,1,1
	bsf	   B2M1M,1
	return	
ValidaBuffer
	banksel	Banderas
	btfsc	TxBuff1, 1
	goto	ReIni1
	return			    ;no se ha llenado buffer. Hacia salida de int.
ReIni1
	btfss	Rebose, 1
	goto	SigReIni1	    ;
	banksel	Banderas
	bcf	Rebose, 1
;
	movf	FSR0H,w, 0	    ;pasa el contenido..
	banksel	TempoH		    
	movwf	TempoH, 1	    ;..rebosado de FSR0 (>0x600)			
	movf	FSR0L,w, 0          ;..y lo respalda..
	banksel	TempoL
	movwf	TempoL, 1	    ;..en TempoN
;
	incfsz	TempoL,1, 1	    ;incrementa TempoN debido a..
	goto	SigRebose	    ;..que se usan las instrucciones..
	incf	TempoH,1, 1	    ;..postinc
;
SigRebose
	lfsr	FSR0,0x200	    ;apunta a inicio de primer buffer
	lfsr	FSR2,0x600
;
AjustaBuff
	movf	POSTINC2,w
	movwf	POSTINC0, 0
;
	banksel	TempoL
	movf	TempoL,w, 1
	subwf	FSR2L,w, 0
	btfss	STATUS,Z, 0
	goto	AjustaBuff
	banksel	TempoH
	movf	TempoH,w, 1
	subwf	FSR2H,w, 0
	btfss	STATUS,Z, 0	
	goto	AjustaBuff
	return				;Sale de int. con el buffer0 ajustado
					;del rebose. Los datos que se grabaron
					;arriba de 0x600 se han copiado al
					;inicio de FSR0=0x200
;
SigReIni1
	lfsr	FSR0,0x200	    ;apunta a inicio de primer buffer
	return			    ;hacia salida de int.
;	
;///////////////////////////////////////////////////////////////////////////////		
;
;///////////////////////////////////////////////////////////////////////////////
IncreEMG
	banksel	C0emgLo
	incfsz	C0emgLo,1, 1
	goto	IncC2
	incfsz	C0emgMd,1, 1
	goto	IncC2
	incf	C0emgHi,1, 1
IncC2
	incfsz	C1emgLo,1, 1
	goto	IncC3
	incfsz	C1emgMd,1, 1	    ;
	goto	IncC3
	incf	C1emgHi,1, 1
IncC3
	incfsz	C2emgLo,1, 1
	return
	incfsz	C2emgMd,1, 1
	return
	incf	C2emgHi,1, 1 
	return	
;///////////////////////////////////////////////////////////////////////////////	
;
;///////////////////////////////////////////////////////////////////////////////
IncreEEG
	banksel	C0eegLo
	incfsz	C0eegLo,1, 1
	goto	IncCE2
	incfsz	C0eegMd,1, 1
	goto	IncCE2
	incf	C0eegHi,1, 1
IncCE2
	incfsz	C1eegLo,1, 1
	goto	IncCE3
	incfsz	C1eegMd,1, 1	    ;
	goto	IncCE3
	incf	C1eegHi,1, 1
IncCE3
	incfsz	C2eegLo,1, 1
	goto	IncCE4
	incfsz	C2eegMd,1, 1
	goto	IncCE4
	incf	C2eegHi,1, 1 
IncCE4
	incfsz	C3eegLo,1, 1
	goto	IncCE5
	incfsz	C3eegMd,1, 1
	goto	IncCE5
	incf	C3eegHi,1, 1
IncCE5
	incfsz	C4eegLo,1, 1
	goto	IncCE6
	incfsz	C4eegMd,1, 1	    ;
	goto	IncCE6
	incf	C4eegHi,1, 1
IncCE6
	incfsz	C5eegLo,1, 1
	return
	incfsz	C5eegMd,1, 1
	return
	incf	C5eegHi,1, 1 
	return
;///////////////////////////////////////////////////////////////////////////////	
;
;///////////////////////////////////////////////////////////////////////////////
IncreIMU
	banksel	M1AXLo
	incfsz	M1AXLo,1, 1	    ;incrementa valor de imu 1 aceler�metro eje x byte bajo
	goto	IncM1AY		    
	incf	M1AXHi,1, 1	    ;incrementa valor de imu 1 aceler�metro eje x byte alto
IncM1AY
	incfsz	M1AYLo,1, 1	    ;incrementa imu 1 aceler�metro eje y byte bajo
	goto	IncM1AZ	    
	incf	M1AYHi,1, 1	    ;incrementa imu 1 aceler�metro eje y byte bajo
IncM1AZ
	incfsz	M1AZLo,1, 1
	goto	IncG
	incf	M1AZHi,1, 1
;
IncG
	incfsz	M1GXLo,1, 1	    ;incrementa imu 1 giroscopio eje x byte bajo
	goto	IncM1GY
	incf	M1GXHi,1, 1	    ;incrementa imu 1 giroscopio eje x byte alto
IncM1GY
	incfsz	M1GYLo,1, 1	    ;incrementa imu 1 giroscopio eje y byte bajo
	goto	IncM1GZ
	incf	M1GYHi,1, 1	    ;incrementa imu 1 giroscopio eje y byte alto
IncM1GZ
	incfsz	M1GZLo,1, 1	    ;incrementa imu 1 giroscopio eje z byte bajo
	goto	IncM
	incf	M1GZHi,1, 1	    ;incrementa imu 1 giroscopio eje z byte alto
;
IncM	
	incfsz	M1MXLo,1, 1	    ;incrementa imu 1 magnet�metro eje x byte bajo
	goto	IncMIMY
	incf	M1MXHi,1, 1	    ;incrementa imu 1 magnet�metro eje x byte alto
IncMIMY
	incfsz	M1MYLo,1, 1	    ;incrementa imu 1 magnet�metro eje y byte bajo
	goto	IncMIMZ
	incf	M1MYHi,1, 1	    ;incrementa imu 1 magnet�metro eje y byte alto
IncMIMZ
	incfsz	M1MZLo,1, 1	    ;incrementa imu 1 magnet�metro eje z byte bajo
	return
	incf	M1MZHi,1, 1	    ;incrementa imu 1 magnet�metro eje z byte alto
	return	
;///////////////////////////////////////////////////////////////////////////////	
	
	

;;**************************************************************************************************
;; Interrupci�n por reset del USB.
;;**************************************************************************************************
;RESET_ISR
;	clrf 	UIR, 0
;	movlw 	b'00101001'			;Interrupciones: Reset, Transacci�n completa y  Stall.
;	movwf 	UIE, 0				;Activa las interrupciones del USB que se requieren.
;	clrf 	UEIR, 0				;Se limpia las banderas de interrupciones de error del USB.
;	clrf 	UADDR, 0			;La direcci�n del dispositivo USB es cero.
;	call	CLR_USTAT_FIFO, 0		;Se limpian las transacciones pendientes.
;	banksel BD0STAT			
;	movlw 	.64
;	movwf 	BD0CNT, 1			;N�mero total de bytes transmitidos/recibidos en el endpoint.
;	movlw 	b'10001000'			
;	movwf 	BD0STAT, 1			;SIE, DATA0, DATA Toggle Synchronization habilitado.
;	clrf	BD1STAT, 1			;Deshabilitado.	
;	bcf	UCON,PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	banksel USB_REG
;	bsf 	USB_REG,DEF, 1			;Ahora el dispositivo USB se encuentra en estado DEFAULT.
;	bcf	USB_REG2,DTSACT, 1		;DATA0 es el actual para el DATA Toggle Synchronization.
;	bcf	UIR,URSTIF, 0			;Se limpia la bandera de interrupci�n por RESET del USB.
;	bcf	PIR2,USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Limpia la FIFO del registro USTAT para no dejar transacciones pendientes.
;;**************************************************************************************************
;CLR_USTAT_FIFO
;	btfss	UIR,TRNIF, 0		
;	return
;	bcf	UIR,TRNIF, 0			;Se limpian las transacciones pendientes.
;	goto 	CLR_USTAT_FIFO
;	return
;
;;**************************************************************************************************
;; Interrupci�n de transferencia completa del USB. Esta rutina solo es v�lida para el EP0.
;;**************************************************************************************************
;TRANS_ISR
;	movlw 	b'01111000'			;M�scara para obtener solo el valor del �ltimo EP que estuvo activo.
;	andwf 	USTAT,0, 0
;	banksel aux_USTAT
;	movwf 	aux_USTAT, 1
;	movlw 	0x00
;	cpfseq 	aux_USTAT, 1			;Compara si la �ltima transacci�n fue en el EP0. Si es 0 salta
;	goto 	TRN_EP1				;Si no, entonces la �ltima transacci�n fue en el EP1.
;	btfsc 	USTAT,DIR, 0			;Se verifica la direcci�n de la �ltima transacci�n. 0= out o setup token; 1=in token
;	goto 	EP0_IN				;se env�a dato a PC 
;EP0_OUT_SETUP
;	banksel BD0STAT
;	movf 	BD0STAT,0, 1
;	andlw 	b'00111100'
;	banksel aux_USTAT
;	movwf 	aux_USTAT, 1
;	movlw 	b'00110100'			;PID(token packet identifier) para SETUP(1101).
;	cpfseq 	aux_USTAT, 1
;	goto 	EP0_OUT				;De no ser un SETUP tiene que ser un OUT por EP0. (la PC envi� dato)
;	goto	EP0_SETUP
;
;;**************************************************************************************************
;; SETUP en el endpoint 0.
;;**************************************************************************************************
;EP0_SETUP	
;	banksel 0x501
;	movlw	.0
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	GET_STATUS			;Si el bRequest = 0 es una solicitud GET_STATUS
;	movlw	.1
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	CLEAR_FEATURE			;Si el bRequest = 1 es una solicitud CLEAR_FEATURE
;	movlw	.3
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SET_FEATURE			;Si el bRequest = 3 es una solicitud SET_FEATURE
;	movlw	.5
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SET_ADDRESS			;Si el bRequest = 5 es una solicitud SET_ADDRESS
;	movlw	.6
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	GET_DESCRIPTOR			;Si el bRequest = 6 es una solicitud GET_DESCRIPTOR
;	movlw	.7
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SET_DESCRIPTOR			;Si el bRequest = 7 es una solicitud SET_DESCRIPTOR
;	movlw	.8
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	GET_CONFIG			;Si el bRequest = 8 es una solicitud GET_CONFIGURATION
;	movlw	.9
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SET_CONFIG			;Si el bRequest = 9 es una solicitud SET_CONFIGURATION
;	movlw	.10
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	GET_INTERFACE			;Si el bRequest = 10 es una solicitud GET_INTERFACE
;	movlw	.11
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SET_INTERFACE			;Si el bRequest = 11 es una solicitud SET_INTERFACE
;	movlw	.12
;	subwf	0x501,0, 1
;	btfsc	STATUS, Z
;	goto	SYNCH_FRAME			;Si el bRequest = 12 es una solicitud SYNCH_FRAME
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1			;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1			;Stall IN del endpoint 0.
;	bcf	UCON,PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR,TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; OUT en el endpoint 0.
;;**************************************************************************************************
;EP0_OUT
;	banksel BD1STAT
;	movlw 	.64
;	movwf 	BD0CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'10001000'
;	movwf	BD0STAT, 1			;SIE, DATA0, DATA Toggle Synchronization habilitado. (OUT del EP0).
;	banksel USB_REG
;	bcf	UIR,TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return	
;
;;**************************************************************************************************
;; IN en el endpoint 0.
;;**************************************************************************************************
;EP0_IN
;	banksel USB_REG
;	btfsc 	USB_REG,ADDAS, 1		;Checa si se debe asignar la nueva direcci�n.
;	goto 	ASIGNAR_ADDRESS
;	banksel BD0STAT
;	movlw 	.64
;	movwf 	BD0CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	banksel USB_REG
;	btfsc	USB_REG,NODS, 1			;Checa si no hay DATA STAGE.
;	movlw 	b'10001000'			;SIE, DATA0, DATA Toggle Synchronization habilitado. (OUT del EP0).
;	btfss	USB_REG,NODS, 1			;Checa si no hay DATA STAGE.
;	movlw 	b'11001000'			;SIE, DATA1, DATA Toggle Synchronization habilitado. (OUT del EP0).
;	banksel BD0STAT
;	movwf	BD0STAT, 1			
;	banksel USB_REG
;	bcf	USB_REG,NODS, 1			;Si hay DATA STAGE. Para utilizarlo en la siguiente solicitud.
;	bcf	UIR,TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;;
;ASIGNAR_ADDRESS
;	banksel 0x502
;	movf 	0x502,0, 1			;0x502 contiene la nueva direcci�n del dispositivo USB.
;	movwf 	UADDR, 0
;	banksel BD0STAT
;	movlw 	.64
;	movwf 	BD0CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'10001000'
;	movwf	BD0STAT, 1			;SIE, DATA0, DATA Toggle Synchronization habilitado. (OUT del EP0).
;	banksel USB_REG
;	bcf 	USB_REG,ADDAS, 1		;No se debe asignar la direcci�n del dispositivo USB.
;	bsf 	USB_REG,ADDR, 1			;Dispositivo en estado ADDRESS.
;	bcf	USB_REG,NODS, 1			;Si hay DATA STAGE. Para utilizarlo en la siguiente solicitud.
;	bcf	UIR,TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Transacci�n en el endpoint 1.
;;**************************************************************************************************
;TRN_EP1
;	btfsc 	USTAT,DIR, 0		;Se verifica la direcci�n de la �ltima transacci�n.
;	goto 	EP1_IN			;env�a datos a la PC
;	goto	EP1_OUT			;recibi� datos de la PC
;
;;**************************************************************************************************		
;; OUT en el endpoint 1.
;;**************************************************************************************************
;EP1_OUT
;	banksel	USB_REG3
;	btfss 	USTAT,PPBI, 0			;Se verifica si el buffer de la �ltima transacci�n fue impar (ping-pong buffering)
;	bcf	USB_REG3,EP1_OUT_PPBI, 1
;	btfsc 	USTAT,PPBI, 0			;Se verifica si el buffer de la �ltima transacci�n fue par (ping-pong buffering)
;	bsf	USB_REG3,EP1_OUT_PPBI, 1
;	btfsc 	USB_REG3,EP1_OUT_PPBI, 1	;Se verifica si el buffer de la �ltima transacci�n fue impar (ping-pong buffering)
;	goto	EP1_OUT_IMPAR
;;
;EP1_OUT_PAR				;Even
;	call 	GET_DATA_EP1_EVEN, 0	;salta a la rutina que lee los datos del buffer USB y los procesa
;	banksel BD2CNT			;BD2 es OUT(par) del EP1.
;	movlw 	.64
;	movwf 	BD2CNT, 1		;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'10001000'		;Pasa control de BD(BufferData) al SIE, DATA0, DATA Toggle Synchronization habilitado. OUT-impar del EP1).
;	movwf	BD2STAT, 1	
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;EP1_OUT_IMPAR				;Odd
;	call 	GET_DATA_EP1_ODD, 0	;salta a la rutina que lee los datos del buffer USB y los procesa
;	banksel BD3CNT			;BD3 es OUT (impar) del EP1.
;	movlw 	.64
;	movwf 	BD3CNT, 1		;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'11001000'		;Pasa control del BD al SIE, DATA1, DATA Toggle Synchronization habilitado. (OUT-par del EP1).
;	movwf	BD3STAT, 1	
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Rutina para leer los 64 datos (Bytes) del buffer par OUT del EP1. Direcci�n de inicio: 0x580.
;;**************************************************************************************************
;GET_DATA_EP1_EVEN
;	banksel TRNF_REG
;	bsf 	TRNF_REG,PC_Dato, 1
;	movlw	.64
;	banksel	NoBytes
;	movwf	NoBytes, 1
;;
;	lfsr	FSR0,0x580		;direcci�n inicial del buffer USB del EP1 para donde llegan los datos provenientes de la PC
;	lfsr	FSR1,0x100		;direcci�n inicial del buffer (grupo de registros) donde se guardan los datos provenientes de la PC
;;
;;Se ejecuta esta rutina para tener siempre en el buffer 0x100-0x13F los datos que mande la PC
;;independientemente si llegan por el buffer par (0x580-0x5BF) o el impar (0x5C0-0x5FF)
;CicGet
;	banksel	POSTINC0
;	movf 	POSTINC0,w, 1
;	movwf	POSTINC1, 1
;	banksel	NoBytes
;	decfsz	NoBytes,1, 1
;	goto	CicGet
;;
;	return
;
;;**************************************************************************************************
;; Rutina para leer los 64 datos (Bytes) del buffer impar OUT del EP1. Direcci�n de inicio: 0x5C0.
;;**************************************************************************************************
;GET_DATA_EP1_ODD
;	banksel TRNF_REG
;	bsf 	TRNF_REG,PC_Dato, 1
;	movlw	.64
;	banksel	NoBytes
;	movwf	NoBytes, 1
;;
;	lfsr	FSR0,0x5C0		;direcci�n inicial del buffer USB del EP1 impar donde llegan los datos provenientes de la PC
;	lfsr	FSR1,0x100		;direcci�n inicial del buffer (grupo de registros) donde se guardan los datos provenientes de la PC
;;
;;Se ejecuta esta rutina para tener siempre en el buffer 0x100-0x13F los datos que mande la PC
;;independientemente si llegan por el buffer par (0x580-0x5BF) o el impar (0x5C0-0x5FF)	
;CicGet1
;	banksel	POSTINC0
;	movf 	POSTINC0,w, 1
;	movwf	POSTINC1, 1
;	banksel	NoBytes
;	decfsz	NoBytes,1, 1
;	goto	CicGet1
;;
;	return
;
;;**************************************************************************************************
;; Enviar datos en el endpoint 1.
;;**************************************************************************************************
;SEND_EP1_DATA
;	banksel USB_REG2
;	btfss	USB_REG2, FRSTIN, 1		;Se verifica si es la primera transacci�n tipo IN. 
;	goto 	PRIMER_IN_EP1			;Si es el primer paquete tipo IN, se configura como paquete tipo DATA0 para su env�o.
;
;	btfss 	USB_REG3, EP1_IN_PPBI, 1	;Se verifica si el buffer de la �ltima transacci�n fue impar (ping-pong buffering)
;	goto	VALIDAR_SYNC_IMPAR
;
;VALIDAR_SYNC_PAR
;	banksel BD4STAT
;	movlw 	.64
;	movwf 	BD4CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	banksel BD4STAT
;	movlw 	b'10001000'			;SIE, DATA0, DATA Toggle Synchronization habilitado. (IN-par del EP1).
;	movwf	BD4STAT, 1
;	banksel USB_REG3
;	bcf 	USB_REG3, EP1_IN_PPBI, 1
;	return
;
;VALIDAR_SYNC_IMPAR
;	banksel BD5STAT
;	movlw 	.64
;	movwf 	BD5CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	banksel BD5STAT
;	movlw 	b'11001000'			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN-impar del EP1).
;	movwf	BD5STAT, 1
;	banksel USB_REG3
;	bsf 	USB_REG3, EP1_IN_PPBI, 1
;	return
;
;PRIMER_IN_EP1	
;	banksel USB_REG2
;	bsf 	USB_REG2,FRSTIN, 1		;Se indica que ya ocurri� el env�o del primer paquete tipo IN.
;	banksel BD4STAT
;	movlw 	.64
;	movwf 	BD4CNT, 1			;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'10001000'			;SIE, DATA0, DATA Toggle Synchronization habilitado. (IN del EP1).
;	movwf	BD4STAT, 1
;	banksel USB_REG3
;	bcf 	USB_REG3,EP1_IN_PPBI, 1
;	return
;
;;**************************************************************************************************
;; IN en el endpoint 1.
;;**************************************************************************************************
;EP1_IN	
;	; call 	SEND_RECEIVED_DATA_USB
;	; banksel 0x5D4
;	; clrf 	0x5D4, 1
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Interrupci�n por ERROR del USB. Esta bandera no puede ser escrita por el usuario.
;;**************************************************************************************************
;ERROR_ISR
;	
;;	bsf 	PORTD, 6, 0
;	goto 	$
;
;;**************************************************************************************************
;; Interrupci�n por actividad en el bus USB.
;;**************************************************************************************************
;ACTIVITY_ISR
;	bcf 	UCON,SUSPND, 0
;CLR_ACT_FLAG
;	btfss 	UIR,ACTVIF, 0	
;	goto	ACT_FLAG_IS_CLR
;	bcf 	UIR,ACTVIF, 0		;La bandera debe esperar a que se estabilice el hardware despu�s de salir del
;	goto 	CLR_ACT_FLAG		;modo SUSPENDED para poder limpiarse.
;ACT_FLAG_IS_CLR
;	bcf 	UIE,ACTVIE, 0		;Se desactiva la interrupci�n por actividad del bus USB.
;	bcf	UIR,ACTVIF, 0		;Se limpia la bandera de interrupci�n por actividad del bus USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;;
;;**************************************************************************************************	
;; Interrupci�n por IDLE del USB.
;;**************************************************************************************************
;IDLE_ISR
;	;bsf 	PORTD, 5, 0
;	goto 	$
;;	
;	bsf 	UIE,ACTVIE, 0		;Activa la interrupci�n por actividad del bus USB.
;	bcf	UIR,IDLEIF, 0		;Se limpia la bandera de interrupci�n por detecci�n de IDLE.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	banksel USB_REG
;	bsf 	USB_REG,SUSP, 1	
;	bsf 	UCON,SUSPND, 0		;Entramos en modo SUSPENDED.
;	return
;
;;**************************************************************************************************
;; Interrupci�n por STALL del USB.
;;**************************************************************************************************
;STALL_ISR
;	banksel BD0STAT
;	movlw	.64
;	movwf 	BD0CNT, 1
;	movlw 	b'10001000'			
;	movwf 	BD0STAT, 1		;SIE, DATA0, DATA Toggle Synchronization habilitado.
;	clrf 	BD1STAT, 1		;Deshabilitado.
;	bcf 	UIR,STALLIF, 0		;Se limpia la bandera de interrupci�n por STALL del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************	
;; Interrupci�n por SOF del USB.
;;**************************************************************************************************
;SOF_ISR
;	;bsf 	PORTD, 3, 0
;	goto 	$
;	
;	bcf 	UIR,SOFIF, 0		;Se limpia la bandera de interrupci�n por SOF del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; 										SOLICITUDES
;;**************************************************************************************************
;; GET_STATUS
;;**************************************************************************************************
;GET_STATUS
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON,PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;;
;	banksel 0x500
;	movf	0x500,0, 1		;Se mueve el valor de bmRequestType(0x500) a aux.
;	banksel aux
;	movwf 	aux, 1
;	movlw	b'00000011'
;	andwf	aux,1, 1
;	movlw	.0
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	DEV_STATUS		;0 - Device
;	movlw	.1
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	INT_STATUS		;1 - Interface
;	movlw	.2
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	EP_STATUS		;2 - Endpoint
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON,PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Se devuelve el estado del dispositivo. Dos bytes se devuelven, el bit 0 es Self-Powered y el 
;; bit 1 Remote Wake-up. Todos los demas son cero.
;;**************************************************************************************************
;DEV_STATUS
;	banksel 0x540
;	movlw	b'00000010'		;Bit 0 = Self-Powered; Bit 1 = Remote Wake-up.
;	movwf	0x540, 1
;	clrf	0x541, 1		;Se mueven los datos en los dos bytes que se tienen que env�ar.		
;	goto	FIN_GET_STATUS
;
;;**************************************************************************************************
;; Se devuelve el estado de la interface. Los dos bytes son cero.
;;**************************************************************************************************
;INT_STATUS
;	banksel 0x540
;	clrf	0x540, 1
;	clrf	0x541, 1		;Se mueven los datos en los dos bytes que se tienen que env�ar.		
;	goto	FIN_GET_STATUS
;
;;**************************************************************************************************
;; Se devuelve el estado del endpoint. Dos bytes se devuelven, el bit 0 es Halt. Todos los demas 
;; son cero.  
;;**************************************************************************************************
;EP_STATUS
;	banksel 0x540
;	movlw	b'00000000'		;Bit 0 = Halt.
;	movwf	0x540, 1
;	clrf	0x541, 1		;Se mueven los datos en los dos bytes que se tienen que env�ar.		
;	goto	FIN_GET_STATUS
;	
;FIN_GET_STATUS
;	banksel BD1STAT
;	movlw	.2
;	movwf 	BD1CNT, 1 
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1		;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON,PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR,TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2,USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; CLEAR_FEATURE
;;**************************************************************************************************
;CLEAR_FEATURE
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;	banksel 0x500
;	movf	0x500, 0, 1		;Se mueve el valor de bmRequestType(0x500) a aux.
;	banksel aux
;	movwf 	aux, 1
;	movlw	b'00000011'
;	andwf	aux,1, 1
;	movlw	.0
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	DEV_CLR_FEATURE		;0 - Device
;	movlw	.1
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	INT_CLR_FEATURE		;1 - Interface
;	movlw	.2
;	subwf	aux,0, 1
;	btfsc	STATUS, Z
;	goto 	EP_CLR_FEATURE		;2 - Endpoint
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; CLEAR_FEATURE para un dispositivo.
;;**************************************************************************************************
;DEV_CLR_FEATURE
;	
;	goto 	FIN_CLEAR_FEATURE
;
;;**************************************************************************************************	
;; CLEAR_FEATURE para una interface.	
;;**************************************************************************************************
;INT_CLR_FEATURE
;	
;	goto 	FIN_CLEAR_FEATURE
;
;;**************************************************************************************************
;; CLEAR_FEATURE para un endpoint.
;;**************************************************************************************************
;EP_CLR_FEATURE
;	
;	goto 	FIN_CLEAR_FEATURE
;
;FIN_CLEAR_FEATURE
;	
;	;bsf 	PORTD, 1, 0
;	;bsf 	PORTD, 7, 0 
;	goto 	$
;
;;**************************************************************************************************
;; SET_FEATURE
;;**************************************************************************************************
;SET_FEATURE
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;	banksel 0x500
;	movf	0x500, 0, 1		;Se mueve el valor de bmRequestType(0x500) a aux.
;	banksel aux
;	movwf 	aux, 1
;	movlw	b'00000011'
;	andwf	aux, 1, 1
;	movlw	.0
;	subwf	aux, 0, 1
;	btfsc	STATUS, Z
;	goto 	DEV_SET_FEATURE		;0 - Device
;	movlw	.1
;	subwf	aux, 0, 1
;	btfsc	STATUS, Z
;	goto 	INT_SET_FEATURE		;1 - Interface
;	movlw	.2
;	subwf	aux, 0, 1
;	btfsc	STATUS, Z
;	goto 	EP_SET_FEATURE		;2 - Endpoint
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; CLEAR_FEATURE para un dispositivo.
;;**************************************************************************************************
;DEV_SET_FEATURE
;	
;	goto 	FIN_SET_FEATURE
;
;;**************************************************************************************************
;; CLEAR_FEATURE para una interface.	
;;**************************************************************************************************
;INT_SET_FEATURE
;	
;	goto 	FIN_SET_FEATURE
;
;;**************************************************************************************************
;; CLEAR_FEATURE para un endpoint.
;;**************************************************************************************************
;EP_SET_FEATURE
;	
;	goto 	FIN_SET_FEATURE
;
;FIN_SET_FEATURE
;	
;	;bsf 	PORTD, 2, 0
;	;bsf 	PORTD, 7, 0 
;	goto 	$
;
;;**************************************************************************************************
;; SET_ADDRESS
;;**************************************************************************************************
;SET_ADDRESS
;	banksel 0x502
;	movlw 	0x00
;	cpfseq 	0x502, 1		;Si la direcci�n es cero entra o se mantiene en estado DEFAULT.	
;	goto 	SET_ADDRESS_ADD
;SET_ADDRESS_DEF				;Ocurri� un error al asignar la direcci�n por lo tanto se regresa al estado DEFAULT.
;	clrf 	UADDR, 0
;	banksel USB_REG
;	bsf 	USB_REG, DEF, 1		;Dispositivo en estado DEFAULT.
;	bcf 	USB_REG, ADDR, 1
;	goto 	FIN_SET_ADDRESS
;SET_ADDRESS_ADD				;Se asigna la direcci�n.
;	banksel USB_REG
;	bsf 	USB_REG, NODS, 1	;No contiene Data Stage
;	bsf 	USB_REG, ADDAS, 1	;Se debe asignar la direcci�n del dispositivo USB.
;FIN_SET_ADDRESS
;	banksel BD1STAT
;	clrf 	BD1CNT, 1
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1		;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; GET_DESCRIPTOR:
;;**************************************************************************************************
;GET_DESCRIPTOR
;	banksel 0x503
;	movlw	.1
;	subwf	0x503, 0, 1
;	btfsc	STATUS, Z
;	goto 	DEV_DES			;1 - DEVICE
;	movlw	.2
;	subwf	0x503, 0, 1
;	btfsc	STATUS, Z
;	goto 	CONFIG_DES		;2 - CONFIGURATION
;	movlw	.3
;	subwf	0x503, 0, 1
;	btfsc	STATUS, Z
;	goto 	STR_DES			;3 - STRING
;	movlw	.4
;	subwf	0x503, 0, 1
;	btfsc	STATUS, Z
;	goto 	INT_DES			;4 - INTERFACE
;	movlw	.5
;	subwf	0x503, 0, 1
;	btfsc	STATUS, Z
;	goto 	EP_DES			;5 - ENDPOINT 
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; SET_DESCRIPTOR
;;**************************************************************************************************
;SET_DESCRIPTOR
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return	
;	
;	;bsf 	PORTD, 3, 0
;	;bsf 	PORTD, 4, 0 
;	;goto 	$
;
;;**************************************************************************************************
;;GET_CONFIGURATION
;;**************************************************************************************************
;GET_CONFIG
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;	
;;	bsf 	PORTD, 4, 0
;;	bsf 	PORTD, 5, 0 
;;	goto 	$
;
;;**************************************************************************************************
;; SET_CONFIGURATION
;;**************************************************************************************************
;SET_CONFIG
;	banksel 0x502
;	movlw 	0x00
;	cpfseq 	0x502, 1
;	goto 	SET_CONFIG_CONF
;SET_CONFIG_ADD
;	banksel USB_REG
;	bsf 	USB_REG, ADDR, 1	;Dispositivo en estado ADDRESS.
;	bcf 	USB_REG, CONF, 1
;	goto 	FIN_SET_CONFIG
;SET_CONFIG_CONF
;	banksel USB_REG
;	bsf 	USB_REG, CONF, 1	;Dispositivo en estado CONFIGURED.
;	movlw 	b'00011110'
;	movwf 	UEP1, 0			;EP1 habilitado para transferencias IN y OUT.
;;
;	banksel BD2STAT
;	movlw	.64
;	movwf	BD2CNT, 1
;	movlw	b'10001000'
;	movwf 	BD2STAT, 1		;SIE, DATA0, DATA Toggle Synchronization habilitado. (OUT-par del EP1).
;;
;	banksel BD3CNT			;BD2 es OUT del EP1.
;	movlw 	.64
;	movwf 	BD3CNT, 1		;M�ximo total de bytes que pueden ser movidos.
;	movlw 	b'11001000'		;Pasa control del BD al SIE, DATA1, DATA Toggle Synchronization habilitado. (OUT-impar del EP1).
;	movwf	BD3STAT, 1	
;;
;	bsf	UCON, PPBRST, 0		;Se colocan todos los buffers ping pong al buffer par.
;	nop
;	bcf	UCON, PPBRST, 0
;
;FIN_SET_CONFIG
;	banksel USB_REG
;	bsf 	USB_REG, NODS, 1	;No contiene Data Stage
;	banksel BD1CNT
;	clrf 	BD1CNT, 1
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1		;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; GET_INTERFACE
;;**************************************************************************************************
;GET_INTERFACE
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; SET_INTERFACE
;;**************************************************************************************************
;SET_INTERFACE
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; SYNCH_FRAME
;;**************************************************************************************************
;SYNCH_FRAME
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1		;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1		;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0		;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; 										DESCRIPTORES
;;**************************************************************************************************
;; Se asigna el Device Descriptor.
;;**************************************************************************************************
;DEV_DES
;	;bsf		PORTB, 0
;	banksel BD1CNT
;	movlw	.18			;18 es la longitud total del Device Descriptor.
;	movwf	BD1CNT, 1
;	banksel 0x507			;0x507 = Parte alta de longitud del descriptor. 0x506 parte baja.
;	movlw	.0
;	cpfseq	0x507, 1		;Compara el registro 0x507(parte alta de longitud del descriptor) si es igual a cero.
;	goto	INI_DEV_DES
;	movlw	.18
;	cpfslt	0x506, 1		;Compara el registro 0x506(parte baja de longitud del descriptor) si es menor 18.
;	goto	INI_DEV_DES
;	movf	0x506,0, 1
;	banksel BD1CNT
;	movwf	BD1CNT, 1
;INI_DEV_DES
;	banksel cont
;	clrf	cont, 1				;cont realiza la funci�n del �ndice del elemento actual que se accesa en la tabla.
;	banksel BD1CNT
;	movf 	BD1CNT,0, 1			;Se mueve el valor de BD1CNT a WREG para asignarlo a aux
;	banksel aux
;	movwf 	aux, 1				;aux contiene la longitud total de datos que se transmitiran/recibiran.
;	movlw 	0x05
;	movwf 	FSR0H, 0
;	movlw 	0x40
;	movwf 	FSR0L, 0			;Se selecciona FSR0 = 0x540, es la direcci�n del Buffer de BD1(IN del EP0).
;LOOP_DEV_DES
;	banksel cont
;	movf 	cont,0, 1
;	addwf	cont,0, 1
;	movwf 	offset, 1			;Se guarda el offset.
;	incf	cont,1, 1
;	call 	DEVICE_DESCRIPTOR, 0
;	movwf	POSTINC0, 0
;	movf 	cont,0, 1
;	cpfseq 	aux, 1				;Compara si cont = aux, si son iguales se sale del ciclo.
;	goto 	LOOP_DEV_DES
;	banksel BD1STAT
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0		        ;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		        ;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Se asigna el Configuration Descriptor.
;;**************************************************************************************************
;CONFIG_DES
;	;bsf 	PORTB, 1, 0
;	banksel BD1CNT
;	movlw	.32
;	movwf	BD1CNT, 1
;	banksel 0x507
;	movlw	.0
;	cpfseq	0x507, 1			;Compara el registro 0x507(parte alta de longitud del descriptor) con cero.
;	goto	INI_CONFIG_DES
;	movlw	.32
;	cpfslt	0x506, 1			;Compara el registro 0x506(parte baja de longitud del descriptor) con 18.
;	goto	INI_CONFIG_DES
;	movf	0x506,0, 1
;	banksel BD1CNT
;	movwf	BD1CNT, 1
;INI_CONFIG_DES
;	banksel cont
;	clrf	cont, 1				;cont realiza la funci�n del �ndice del elemento actual que se accesa en la tabla.
;	banksel BD1CNT
;	movf 	BD1CNT,0, 1			;Se mueve el valor de BD1CNT a WREG para asignarlo a aux
;	banksel aux
;	movwf 	aux, 1				;aux contiene la longitud total de datos que se transmitiran/recibiran.
;	movlw 	0x05
;	movwf 	FSR0H, 0
;	movlw 	0x40
;	movwf 	FSR0L, 0			;Se selecciona FSR0 = 0x540, es la direcci�n del Buffer de BD1(IN del EP0).
;LOOP_CONFIG_DES
;	banksel cont
;	movf 	cont,0, 1
;	addwf	cont,0, 1
;	movwf 	offset, 1			;Se guarda el offset.
;	incf	cont, 1, 1
;	call 	CONFIGURATION_DESCRIPTOR, 0
;	movwf	POSTINC0,0
;	movf 	cont,0, 1
;	cpfseq 	aux, 1				;Compara si cont = aux, si son iguales se sale del ciclo.
;	goto 	LOOP_CONFIG_DES
;	banksel BD1STAT
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Rutina para seleccionar el STRING DESCRIPTOR adecuado.
;;**************************************************************************************************
;STR_DES
;	;bsf 	PORTB, 2, 0
;	banksel 0x502
;	movlw	.0
;	subwf	0x502,0, 1		
;	btfsc	STATUS, Z
;	goto	STR0_DES			;Indice 0
;	movlw	.1
;	subwf	0x502,0, 1		
;	btfsc	STATUS, Z
;	goto	STR1_DES			;Indice 1
;	movlw	.2
;	subwf	0x502,0, 1		
;	btfsc	STATUS, Z
;	goto	STR2_DES			;Indice 2
;	banksel BD0STAT
;	movlw	b'10000100'
;	movwf 	BD0STAT, 1			;Stall OUT del endpoint 0.
;	movwf	BD1STAT, 1			;Stall IN del endpoint 0.
;	bcf	UCON, PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Se asigna el String Descriptor para el String 0.
;;**************************************************************************************************
;STR0_DES
;	banksel BD1CNT
;	movlw	.6
;	movwf	BD1CNT, 1
;	banksel aux
;	movwf 	aux, 1				;aux contiene la longitud total de datos que se transmitiran/recibiran.
;	clrf	cont, 1				;cont realiza la funci�n del �ndice del elemento actual que se accesa en la tabla.
;	movlw 	0x05
;	movwf 	FSR0H, 0
;	movlw 	0x40
;	movwf 	FSR0L, 0			;Se selecciona FSR0 = 0x540, es la direcci�n del Buffer de BD1(IN del EP0).
;LOOP_STR0_DES
;	banksel cont
;	movf 	cont,0, 1
;	addwf	cont,0, 1
;	movwf 	offset, 1			;Se guarda el offset.
;	incf	cont, 1, 1
;	call 	STRING_DESCRIPTOR, 0
;	movwf	POSTINC0, 0
;	movf 	cont,0, 1
;	cpfseq 	aux, 1				;Compara si cont = aux, si son iguales se sale del ciclo.
;	goto 	LOOP_STR0_DES
;	banksel BD1STAT
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Se asigna el String Descriptor para el String 1.
;;**************************************************************************************************
;STR1_DES
;	banksel BD1CNT
;	movlw	.16
;	movwf	BD1CNT, 1
;	banksel aux
;	movwf 	aux, 1				;aux contiene la longitud total de datos que se transmitiran/recibiran.
;	movlw 	.6
;	movwf	cont, 1				;cont realiza la funci�n del �ndice del elemento actual que se accesa en la tabla.
;	addwf 	aux, 1, 1
;	movlw 	0x05
;	movwf 	FSR0H, 0
;	movlw 	0x40
;	movwf 	FSR0L, 0			;Se selecciona FSR0 = 0x540, es la direcci�n del Buffer de BD1(IN del EP0).
;LOOP_STR1_DES
;	banksel cont
;	movf 	cont,0, 1
;	addwf	cont,0, 1
;	movwf 	offset, 1			;Se guarda el offset.
;	incf	cont, 1, 1
;	call 	STRING_DESCRIPTOR, 0
;	movwf	POSTINC0, 0
;	movf 	cont,0, 1
;	cpfseq 	aux, 1				;Compara si cont = aux, si son iguales se sale del ciclo.
;	goto 	LOOP_STR1_DES
;	banksel BD1STAT
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0		       ;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0		       ;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0		       ;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; Se asigna el String Descriptor para el String 2.
;;**************************************************************************************************
;STR2_DES
;	banksel BD1CNT
;	movlw	.32
;;+++
;	movwf	BD1CNT, 1
;	banksel aux
;	movwf 	aux, 1				;aux contiene la longitud total de datos que se transmitiran/recibiran.
;	movlw 	.22
;	movwf	cont, 1				;cont realiza la funci�n del �ndice del elemento actual que se accesa en la tabla.
;	addwf 	aux,1, 1
;	movlw 	0x05
;	movwf 	FSR0H, 0
;	movlw 	0x40
;	movwf 	FSR0L, 0			;Se selecciona FSR0 = 0x540, es la direcci�n del Buffer de BD1(IN del EP0).
;LOOP_STR2_DES
;	banksel cont
;	movf 	cont,0, 1
;	addwf	cont,0, 1
;	movwf 	offset, 1			;Se guarda el offset.
;	incf	cont,1, 1
;	call 	STRING_DESCRIPTOR, 0
;	movwf	POSTINC0, 0
;	movf 	cont,0, 1
;	cpfseq 	aux, 1				;Compara si cont = aux, si son iguales se sale del ciclo.
;	goto 	LOOP_STR2_DES
;	banksel BD1STAT
;	movlw 	b'11001000'			
;	movwf 	BD1STAT, 1			;SIE, DATA1, DATA Toggle Synchronization habilitado. (IN del EP0).
;	bcf	UCON, PKTDIS, 0			;Habilita el procesamiento de paquetes y tokens por el SIE.
;	bcf	UIR, TRNIF, 0			;Se limpia la bandera de interrupci�n de transacci�n completa del USB.
;	bcf	PIR2, USBIF, 0			;Se limpia la bandera de interrupci�n del USB.
;	return
;
;;**************************************************************************************************
;; 
;;**************************************************************************************************
;INT_DES
;	;bsf 	PORTD, 2, 0
;	;bsf 	PORTD, 3, 0 
;	;bsf 	PORTD, 4, 0 
;	goto 	$
;
;;**************************************************************************************************
;; 
;;**************************************************************************************************
;EP_DES
;
;	;bsf 	PORTD, 3, 0
;	;bsf 	PORTD, 4, 0 
;	;bsf 	PORTD, 5, 0 
;	goto 	$
;
;;**************************************************************************************************
;;								TABLAS DE DESCRIPTORES
;;**************************************************************************************************
;; Device Descriptor
;;**************************************************************************************************
;	ORG 0x7000
;DEVICE_DESCRIPTOR	
;	movf 	PCL,0, 0		;Se lee el PCL para obtener los valores de PCLATH y PCLATU
;	banksel offset
;	movf 	offset,0, 1
;	addwf	PCL,1, 0		;Offset al contador de programa.
;	;******************************Device Descriptor*************************************
;	retlw	.18			;bLength(1 byte): Size of this descriptor in bytes.
;	retlw	.1			;bDescriptorType(1 byte): DEVICE(1) Descriptor Type.
;	retlw	0x00			;bcdUSB(2 bytes): USB Specification Release Number in Binary-Coded Decimal.
;	retlw	0x02			;	 (i.e., 2.10 is 210H).
;	retlw	.0				;bDeviceClass(1 byte): Class code. If this field is reset to zero, each interface within a configuration
;							;	specifies its own class information and the various interfaces operate independently.
;							;	If this field is set to a value between 1 and FEH, the device supports different class
;							;	specifications on different interfaces and the interfaces may not operate independently. This 
;							;	value identifies the class definition used for the aggregate interfaces.
;							;	If this field is set to FFH, the device class is vendor-specific.
;	retlw	.0				;bDeviceSubClass(1 byte): Subclass code. If the bDeviceClass field is reset to zero, this field must 
;							;	also be reset to zero. If the bDeviceClass field is not set to FFH, all values are reserved for 
;							;	assignment by the USB-IF.
;	retlw	.0				;bDeviceProtocol(1 byte): Protocol code. If this field is reset to zero, the device does not use 
;							;	class-specific protocols on a device basis.  However, it may use class-specific protocols on an 
;							;	interface basis. If this field is set to FFH, the device uses a vendor-specific protocol on a device basis.
;	retlw	.64				;bMaxPacketSize0(1 byte): Maximum packet size for endpoint zero(only 8, 16, 32, or 64 are valid).
;	retlw	0xD8			;idVendor(2 bytes): Vendor ID (assigned by the USB-IF).
;	retlw 	0x04
;	retlw 	0x53			;idProduct(2 bytes): Product ID (assigned by the manufacturer). 22h
;	retlw 	0x00
;	retlw 	0x01			;bcdDevice(2 bytes): Device release number in binary-coded decimal.
;	retlw 	0x00
;	retlw 	.1			;iManufacturer(1 byte): Index of string descriptor describing manufacturer.
;	retlw	.2			;iProduct(1 byte): Index of string descriptor describing product.
;	retlw	.0			;iSerialNumber(1 byte): Index of string descriptor describing the device's serial number.
;	retlw	.1			;bNumConfigurations(1 byte): Number of possible configurations.
;
;;**************************************************************************************************
;; Configuration Descriptor
;;**************************************************************************************************
;	ORG 0x7100
;CONFIGURATION_DESCRIPTOR
;	movf 	PCL,0, 0			;Se lee el PCL para obtener los valores de PCLATH y PCLATU
;	banksel offset
;	movf 	offset,0, 1
;	addwf	PCL,1, 0			;Offset al contador de programa.
;	;******************************Configuration_Descriptor*************************************
;	retlw 	.9				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.2				;bDescriptorType(1 byte): CONFIGURATION(2) Descriptor Type.
;	retlw 	.32				;wTotalLength(2 bytes): Total length of data returned for this configuration.  Includes the combined 
;	retlw 	0x00			;	length of all descriptors (configuration, interface, endpoint, and class- or vendor-specific)
;							;	returned for this configuration.
;	retlw 	.1				;bNumInterfaces(1 byte): Number of interfaces supported by this configuration.
;	retlw 	.1				;bConfigurationValue(1 byte): Value to use as an argument to the SetConfiguration() request 
;							;	to select this configuration.
;	retlw 	.0				;iConfiguration(1 byte): Index of string descriptor describing this configuration.
;	retlw 	b'10100000'			;bmAttributes(1 byte): Configuration characteristics
;							;		D7: Reserved (set to one)
;							;		D6: Self-powered if D6=1
;							;		D5: Remote Wakeup
;							;		D4...0: Reserved (reset to zero)
;							;	D7 is reserved and must be set to one for historical reasons.
;							;	A device configuration that uses power from the bus and a local source reports a non-zero
;							;	value in bMaxPower to indicate the amount of bus power required and sets D6.  The actual
;							;	power source at runtime may be determined using the GetStatus(DEVICE) request.
;							;	If a device configuration supports remote wakeup, D5 is set to one.
;	retlw 	.50				;bMaxPower(1 byte): Maximum power consumption of the USB device from the bus in this specific
;							;	configuration when the device is fully operational.  Expressed in 2 mA units.
;							;	(i.e., 50 = 100 mA).
;
;;**************************************************************************************************
;; Interface Descriptor
;;**************************************************************************************************
;	retlw 	.9				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.4				;bDescriptorType(1 byte): INTERFACE(4) Descriptor Type.
;	retlw 	.0				;bInterfaceNumber(1 byte): Number of this interface. Zero-based value identifying the index in 
;							;	the array of concurrent interfaces supported by this configuration.
;	retlw 	.0				;bAlternateSetting(1 byte): Value used to select this alternate setting for the interface 
;							;	identified in the prior field.
;	retlw 	.2				;bNumEndpoints(1 byte): Number of endpoints used by this interface (excluding endpoint zero).  
;							;	If this value is zero, this interface only uses the Default Control Pipe.
;	retlw 	0xFF				;bInterfaceClass(1 byte): Class code. A value of zero is reserved for future standardization.
;							;	If this field is set to FFH, the interface class is vendor-specific.
;							;	All other values are reserved for assignment by the USB-IF.
;	retlw 	0xFF				;bInterfaceSubClass(1 byte): Subclass code. If the bInterfaceClass field is reset to zero,
;							;	this field must also be reset to zero. If the bInterfaceClass field is not set to FFH, 
;							;	all values are reserved for assignment by the USB-IF.
;	retlw 	0xFF				;bInterfaceProtocol(1 byte): Protocol code. f this field is reset to zero, the device
;							;	does not use a class-specific protocol on this interface. If this field is set to FFH, 
;							;	the device uses a vendor-specific protocol for this interface.
;	retlw 	.0				;iInterface(1 byte): Index of string descriptor describing this interface.
;
;;**************************************************************************************************
;; Endpoint 1 (OUT) Descriptor
;;**************************************************************************************************
;	retlw 	.7				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.5				;bDescriptorType(1 byte): ENDPOINT(5) Descriptor Type.
;	retlw 	0x01				;bEndpointAddress(1 byte): The address of the endpoint on the USB device described by this 
;							;	descriptor.  The address is encoded as follows:
;							;		Bit 3..0:  The endpoint number
;							;		Bit 6..4:  Reserved, reset to zero
;							;		Bit 7:     Direction, ignored forcontrol endpoints
;							;	         0 = OUT endpoint
;							;	         1 = IN endpoint
;	retlw 	0x02				;bmAttributes(1 byte): This field describes the endpoint?s attributes when it is configured
;							;	using the bConfigurationValue.
;							;		Bits 1..0:  Transfer Type
;							;		    00 = Control
;							;		    01 = Isochronous
;							;		    10 = Bulk
;							;		    11 = Interrupt
;							;	If not an isochronous endpoint, bits 5..2 are reserved and must be set to zero.
;	retlw 	0x40				;wMaxPacketSize(2 bytes): Maximum packet size this endpoint is capable of sending or 
;	retlw	0x00				;receiving when this configuration is selected.
;							;	For all endpoints, bits 10..0 specify the maximum packet size (in bytes).
;	retlw 	0x01				;bInterval(1 byte): Interval for polling endpoint for data transfers (Only Isochronous).
;							;	For Full-Speed Bulk and control transfers, the value is ignore.
;
;;**************************************************************************************************
;; Endpoint 1 (IN) Descriptor
;;**************************************************************************************************				
;	retlw 	.7				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.5				;bDescriptorType(1 byte): ENDPOINT(5) Descriptor Type.
;	retlw 	0x81			;bEndpointAddress(1 byte): The address of the endpoint on the USB device described by this 
;							;	descriptor.  The address is encoded as follows:
;							;		Bit 3..0:  The endpoint number
;							;		Bit 6..4:  Reserved, reset to zero
;							;		Bit 7:     Direction, ignored forcontrol endpoints
;							;	         0 = OUT endpoint
;							;	         1 = IN endpoint
;	retlw 	0x02			;bmAttributes(1 byte): This field describes the endpoint?s attributes when it is configured
;							;	using the bConfigurationValue.
;							;		Bits 1..0:  Transfer Type
;							;		    00 = Control
;							;		    01 = Isochronous
;							;		    10 = Bulk
;							;		    11 = Interrupt
;							;	If not an isochronous endpoint, bits 5..2 are reserved and must be set to zero.
;	retlw 	0x40			;wMaxPacketSize(2 bytes): Maximum packet size this endpoint is capable of sending or 
;	retlw	0x00			;	receiving when this configuration is selected.
;							;	For all endpoints, bits 10..0 specify the maximum packet size (in bytes).
;	retlw 	0x01			;bInterval(1 byte): Interval for polling endpoint for data transfers (Only Isochronous).
;							;	For Full-Speed Bulk and control transfers, the value is ignore.
;
;;**************************************************************************************************
;; String Descriptor
;;**************************************************************************************************
;	ORG 0x7200
;STRING_DESCRIPTOR
;	movf 	PCL,0, 0		;Se lee el PCL para obtener los valores de PCLATH y PCLATU
;	banksel offset
;	movf 	offset,0, 1
;	addwf	PCL,1, 0		;Offset al contador de programa.
;	;*********************String_Descriptor*****************************
;	;String 0:
;	retlw 	.6				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.3				;bDescriptorType(1 byte): STRING(3) Descriptor Type.
;	retlw 	0x09			    ;wLANGID[0](2 bytes): LANGID code zero. 0x0409 = Ingl�s.
;	retlw 	0x04			
;	retlw 	0x0A			    ;wLANGID[1](2 bytes): LANGID code one. 0x080A = Espa�ol mexicano.
;	retlw 	0x08			
;	
;	;String 1:
;	retlw 	.16				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.3				;bDescriptorType(1 byte): STRING(3) Descriptor Type.
;	retlw 	'T'				;bString(N bytes): UNICODE encoded string.
;	retlw 	.0
;	retlw 	'e'
;	retlw 	.0
;	retlw 	'l'
;	retlw 	.0
;	retlw 	'e'
;	retlw 	.0
;	retlw 	'_'
;	retlw 	.0
;	retlw 	'B'
;	retlw 	.0
;	retlw 	'S'
;	retlw 	.0
;	
;	;String 2:
;	retlw 	.24				;bLength(1 byte): Size of this descriptor in bytes.
;	retlw 	.3				;bDescriptorType(1 byte): STRING(3) Descriptor Type.
;	retlw 	'T'				;bString(N bytes): UNICODE encoded string.
;	retlw 	.0
;	retlw 	'e'
;	retlw 	.0
;	retlw 	'l'
;	retlw 	.0
;	retlw 	'e'
;	retlw 	.0
;	retlw 	'_'
;	retlw 	.0
;	retlw 	'B'
;	retlw 	.0
;	retlw 	'S'
;	retlw 	.0
;	retlw 	' '
;	retlw 	.0
;	retlw 	'U'
;	retlw 	.0
;	retlw 	'S'
;	retlw 	.0
;	retlw 	'B'
;	retlw 	.0				
;
;;**************************************************************************************************
;;
;-----------------------------------------Retardos-------------------------------------------------
;
;******************************************************************************
;Retardo utilizando 48MHZ de clock/256=187,500Hz. DH guarda la parte alta del timer 0 para 
;asignar. DL guarda la parte baja.
;******************************************************************************
; DELAY
; 	movlw 	b'00000111'
; 	movwf 	T0CON, 0
; 	banksel DH
; 	movf 	DH,0, 1				;Se mueve el valor de la parte alta del timer 0.
; 	movwf 	TMR0H, 0			
; 	movf 	DL,0, 1				;Se mueve el valor de la parte baja del timer 0.
; 	movwf 	TMR0L, 0
; 	bcf 	INTCON,TMR0IF, 0		;Limpia la bandera del timer 0.
; 	bsf 	T0CON,TMR0ON, 0			;Se enciende el timer 0.
; DESBORDE
; 	btfss 	INTCON,TMR0IF, 0		;Checa si se desbord� el timer 0.
; 	goto 	DESBORDE
; 	bcf 	INTCON, TMR0IF, 0		;Limpia la bandera del timer 0.
; 	return

;**************************************************************************************************
; Retardo de aprox. 65 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
;Delay65ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xF4 				;Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 3047 (0xF418).
;	movwf 	TMR0H, 0			
;	movlw 	0x18				;Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		;Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			;Se enciende el timer 0.
;OVERFLOW_65ms
;	btfss 	INTCON,TMR0IF, 0		;Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_65ms
;	bcf 	INTCON,TMR0IF, 0		;Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 4us.Clock(48MHz)/2=24MHz
;**************************************************************************************************
;Delay4us
;	movlw 	b'00000000'
;	movwf 	T0CON, 0	
;	movlw 	0xFF 				;Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 47 (0xFFD0).
;	movwf 	TMR0H, 0			
;	movlw 	0xA0				;Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		;Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			;Se enciende el timer 0.
;OVERFLOW_4us
;	btfss 	INTCON,TMR0IF, 0		;Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_4us
;	bcf 	INTCON, TMR0IF, 0		;Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 1 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
;Delay1ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xFF 				;Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 47 (0xFFD0).
;	movwf 	TMR0H, 0			
;	movlw 	0xD0				;Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		;Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			;Se enciende el timer 0.
;OVERFLOW_1ms
;	btfss 	INTCON,TMR0IF, 0		;Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_1ms
;	bcf 	INTCON, TMR0IF, 0		;Limpia la bandera del timer 0.
;	return
;
;;**************************************************************************************************
;; Retardo de aprox. 5 ms.Clock(48MHz)/256=187,500Hz
;;**************************************************************************************************
;Delay5ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xFF 				; Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 234 (0xFF15).
;	movwf 	TMR0H, 0			
;	movlw 	0x15				; Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
;OVERFLOW_5ms
;	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_5ms
;	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 10 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************	
;Delay10ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xFE 				; Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 234 (0xFF15).
;	movwf 	TMR0H, 0			
;	movlw 	0x2A				; Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
;OVERFLOW_10ms
;	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_10ms
;	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 15 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
;Delay15ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xF5 				; Se mueve el valor de la parte alta del timer 0.
;	movwf 	TMR0H, 0			
;	movlw 	0x03				; Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
;OVERFLOW_15ms
;	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_15ms
;	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 100 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
Delay100ms
	movlw 	b'00000111'
	movwf 	T0CON, 0	
	movlw 	0xED 				; Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 234 (0xFF15).
	movwf 	TMR0H, 0			
	movlw 	0xAF				; Se mueve el valor de la parte baja del timer 0.
	movwf 	TMR0L, 0
	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
OVERFLOW_100ms
	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
	goto 	OVERFLOW_100ms
	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
	return

;**************************************************************************************************
; Retardo de aprox. 200 ms.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
;Delay200ms
;	movlw 	b'00000111'
;	movwf 	T0CON, 0	
;	movlw 	0xDB 				; Se mueve el valor de la parte alta del timer 0. El contador debe llegar a 234 (0xFF15).
;	movwf 	TMR0H, 0			
;	movlw 	0x60				; Se mueve el valor de la parte baja del timer 0.
;	movwf 	TMR0L, 0
;	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
;	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
;OVERFLOW_200ms
;	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
;	goto 	OVERFLOW_200ms
;	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
;	return

;**************************************************************************************************
; Retardo de aprox. 1s.Clock(48MHz)/256=187,500Hz
;**************************************************************************************************
Delay1s
	movlw 	b'00000111'
	movwf 	T0CON, 0	
	movlw 	0x48 				; Se mueve el valor de la parte alta del timer 0. Valor para un segundo 0x48E4.
	movwf 	TMR0H, 0			
	movlw 	0xE4				; Se mueve el valor de la parte baja del timer 0.
	movwf 	TMR0L, 0
	bcf 	INTCON,TMR0IF, 0		; Limpia la bandera del timer 0.
	bsf 	T0CON,TMR0ON, 0			; Se enciende el timer 0.
OVERFLOW_1s
	btfss 	INTCON,TMR0IF, 0		; Checa si se desbord� el timer 0.
	goto 	OVERFLOW_1s
	bcf 	INTCON, TMR0IF, 0		; Limpia la bandera del timer 0.
	return

;**************************************************************************************************
; Configura el Timer 1 para interrumpir al uC.Base de tiempo = Fosc/4= 48MHz/4= 12MHz
; Se programa para contar 3,000 pulsos de reloj (0x0BB8) e interrumpir
;**************************************************************************************************
INIT_TMR1 
;	banksel cont_tmr1
;	movlw	.233				; Se utiliza cont_tmr1 para contar x desbordes del timer 1 (~1 segundo)
;	movwf	cont_tmr1, 1
;	banksel Menu
;	bsf	Menu, TMR1_RUN, 1
;	bsf	Menu, CLR_TMR1, 1
;	
	movlw 	b'10110000'
	movwf 	T1CON, 0			;Rd/Wr 16b, preesc=1:8,internal clock=12MHz, tmr1 off. Avanza una cuenta cada 0.67us
	movlw 	0xF4				;0x0B		
	movwf 	TMR1H, 0			; Se mueve el valor de la parte alta del timer 1.
	movlw 	0x47				;0xB8			
	movwf 	TMR1L, 0			; Se mueve el valor de la parte baja del timer 1.
	bcf 	PIR1, TMR1IF, 0			; Limpia la bandera del timer 1.
	bsf 	PIE1, TMR1IE, 0			; Se habilita la interrupci�n por desborde del timer 1.
	bsf 	T1CON, TMR1ON, 0		; Se enciende el timer 1.
	movlw 	b'11000000'
	movwf 	INTCON, 0			; Se habilitan interrupciones globales y de perif�ricos
	return

	end
	